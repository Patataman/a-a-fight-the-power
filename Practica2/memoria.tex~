\documentclass[10pt,a4paper,titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[official]{eurosym}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage[table]{xcolor}
\usepackage{geometry}
\usepackage{pgfplots}
\geometry{top=2.54cm, left=3.1cm, right=3.1cm, bottom=2.54cm}
\setlength{\parskip}{0.5em}
\usepackage{hyperref}
\usepackage{fancyref}

\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}



\title{ \textbf{ \Huge{Memoria Práctica 2: Aprendizaje basado en instancias}} \\ Aprendizaje Automático}
\author{
		\begin{tabular}{lr}
			\multicolumn{1}{l}{Grupo 83} \\ \cline{1-1} \\
			Daniel Alejandro Rodríguez López & 100316890 \\
			Adrián Borja Pimentel & 100315536 \\
		\end{tabular}
}



\begin{document}
\maketitle
\tableofcontents
\newpage
\setcounter{secnumdepth}{0}
\section{Introducción}
	El objetivo de esta práctica consiste en realizar un nuevo agente automático basado en aprendizaje no supervisado, concretamente usando clústeres.\\ Para ello se ha realizado sobre un conjunto inicial de instancias una serie de pruebas con distintos algoritmos de clusterización. En base al análisis de los resultados se elige uno de estos algoritmos y un modelo de clúster en específico y sobre éste se realiza la implementación.

	ASDFUAGFSDAUYAFSUYASUFGASUGIUAS

\section{Recogida de información}
	Nuestro trabajo en esta sección consistía en modificar en el agente la función encargada de recoger los datos que forman las instancias, se nos pedía que dichos datos fueran del tipo \textit{info\_estado, acción, info \_estado+n}, nuestro \textit{info\_estado} está compuesto por: 

	\begin{itemize}
		\item \textbf{lightYearsAzul:} numeric
		\item \textbf{lightYearsNaranja:} numeric
		\item \textbf{lightYearsCian:} numeric
		\item \textbf{lightYearsMandarina:} numeric
		\item \textbf{patrullaX:} numeric
		\item \textbf{patrullaY:} numeric
		\item \textbf{scoreNow:} numeric
		\item \textbf{legalUp:} numeric
		\item \textbf{legalDown:} numeric
		\item \textbf{legalLenin:} numeric
		\item \textbf{legalFranco:} numeric
		
	\end{itemize}

	Al final de la instancia se encuentra el atributo:
	\begin{itemize}
		\item \textbf{funcion1:}
	\end{itemize}

	la distancia a los fantasmas, la posición del Pac-man en el tablero, la puntuación actual, los movimientos legales, la fórmula 1 que se menciona en el enunciado implementada en la función \textit{funcion1}, y la dirección a la que se desplaza el pacman.

	Utilizaremos datos extraídos de los mapas \textit{bigHunt}, \textit{mediumClassic} y \textit{contestClassic}.

\section{Clustering}

	AÑADIR JUSTIFICACIÓN DE USO DE ALGORITMOS (AUNQUE NO SE QUE ESPERAN QUE JUSTIFIQUEMOS SI NOS DICEN ELLOS QUE USEMOS ESTOS)

	En esta fase usaremos los algoritmos \textit{simplekmeans}, \textit{EM} y \textit{xmeans}, y guardaremos los modelos y buffers de resultado generados en el directorio «clusters», donde habrá otro directorio dedicado a cada algoritmo.

	En cuanto a las instancias que usaremos, retiraremos todos los atributos de \textit{info \_estado+n}, \textit{funcion1}, \textit{fate}, y \textit{score}, ya que los primeros son datos del futuro, otro es la clase y \textit{score} nos obligaría a normalizar los datos ya que es demasiado inestable y consideramos que apenas aporta información útil. Hemos eliminado también las instancias que pertenecen al momento en el que un fantasma es comido ya que produce un poco de incertidumbre.

	Los datos se representarán haciendo uso de gráficas, de esta forma las variaciones de proporciones entre las distintas pruebas se aprecian mejor.

	\subsection{\textit{EM}}
		Nombraremos cada prueba con la semilla que hemos usado, ya que para el número máximo de iteraciones siempre usaremos 500, que es un número suficientemente grande como para que el resultado obtenido sea porque el algoritmo ha llegado a un resultado estable. Los resultados de estas pruebas se pueden encontrar en la ruta «cluster/EM/buffer» y «cluster/EM/models».

		Debido al gran número de clústeres que se alcance en algunas pruebas se mostraran textualmente los resultados.

		\begin{itemize}
			\item \textbf{64: } Se han generado 11 clusters de proporciones: 8\%, 7\%, 45\%, 5\%, 7\%, 6\%, 9\%, 1\%, 2\%, 8\%, 2\%.

			\item \textbf{100: } Se han generado 7 clusters de proporciones: 38\%, 9\%, 12\%, 3\%, 8\%, 13\%, 18\%.

			\item \textbf{128: } Se han generado 6 clusters de proporciones: 43\%, 15\%, 6\%, 2\%, 15\%, 19\%.

			\item \textbf{256: } Se han generado 6 clusters de proporciones: 10\%, 27\%, 25\%, 4\%, 10\%, 23\%.

			\item \textbf{512: } Se han generado 7 clusters de proporciones: 5\%, 14\%, 5\%, 2\%, 14\%, 17\%, 43\%.

			\item \textbf{666: } Se han generado 9 clusters de proporciones: 8\%, 6\%, 1\%, 5\%, 11\%, 5\%, 8\%, 10\%, 47\%.

			\item \textbf{1024: } Se han generado 4 clusters de proporciones: 3\%, 15\%, 51\%, 31\%.

			\item \textbf{1758: } Se han generado 4 clusters de proporciones: 15\%, 51\%, 3\%, 31\%.

		\end{itemize}

		En general vemos que EM no es nada estable, el número de clusters cambia casi en cada prueba y apenas están equilibrados, lo más destacable es que las dos últimas pruebas han coincidido en 4 clusters y aunque cambiados, las proporciones son idénticas.

	\subsection{\textit{xmeans}}
		Nombraremos cada prueba con el formato `Nºcluster\_semilla', fijaremos el maxIterations a 500 y dejaremos el resto de parámetros por defecto. Los resultados de estas pruebas se pueden encontrar en la ruta «cluster/xmeans/buffer» y «cluster/xmeans/models».

		\begin{figure}[ht!]
		%\centering
		\begin{tikzpicture}
			\begin{axis}[
			 	title={Proporciones 4 clústeres},
				ybar,
				symbolic x coords={64,100,128,256,512,666,1024,1758},
				xtick=data,
				ylabel={Porcentaje},
				bar width=4,
				ymax=50,
				enlargelimits=0.07,
				legend style={at={(0.5,-0.15)},
				anchor=north,legend columns=-1},
				ymajorgrids = true,
				enlarge x limits={abs=1cm},
				x tick label style={rotate=40, anchor=east, align=right,text width=1.5cm},
				ylabel near ticks,
				nodes near coords,
				width = 17cm,
				height = 8cm
			]
			\addplot [fill=gray!25] coordinates {(64, 34) (100, 21) (128, 34) (256, 10) (512, 31) (666, 35) (1024, 35) (1758, 26)};
			\addplot [fill=blue] 	coordinates {(64, 24) (100, 14) (128, 12) (256, 17) (512, 28) (666, 47) (1024, 47) (1758, 36)};
			\addplot [fill=red] 	coordinates {(64, 18) (100, 29) (128, 38) (256, 44) (512, 24) (666, 10) (1024, 10) (1758, 15)};
			\addplot [fill=green] 	coordinates {(64, 24) (100, 36) (128, 16) (256, 30) (512, 16) (666, 7)  (1024, 7)  (1758, 22)};

			\legend{Cluster 1, Cluster 2, Cluster 3, Cluster 4}
			\end{axis}
		\end{tikzpicture}
		\caption{Xmeans, K=4}
		\end{figure}

		\begin{figure}[ht!]
		%\centering
		\begin{tikzpicture}
			\begin{axis}[
			 	title={Proporciones 6 clústeres},
				ybar,
				symbolic x coords={64,100,128,256,512,666,1024,1758},
				xtick=data,
				ylabel={Porcentaje},
				bar width=4,
				ymax=37,
				enlargelimits=0.07,
				legend style={at={(0.5,-0.15)},
				anchor=north,legend columns=-1},
				ymajorgrids = true,
				enlarge x limits={abs=1cm},
				x tick label style={rotate=40, anchor=east, align=right,text width=1.5cm},
				ylabel near ticks,
				nodes near coords,
				width = 17cm,
				height = 8cm
			]
			\addplot [fill=gray!25] coordinates {(64, 7)  (100, 14) (128, 27) (256, 10) (512, 25) (666, 28)  (1024, 15) (1758, 23)};
			\addplot [fill=blue] 	coordinates {(64, 26) (100, 12) (128, 12) (256, 8)  (512, 16) (666, 7)   (1024, 11) (1758, 10)};
			\addplot [fill=red] 	coordinates {(64, 15) (100, 27) (128, 26) (256, 21) (512, 5)  (666, 25)  (1024, 10) (1758, 8)};
			\addplot [fill=green] 	coordinates {(64, 6)  (100, 14) (128, 12) (256, 26) (512, 15) (666, 26)  (1024, 7)  (1758, 21)};
			\addplot [fill=white] 	coordinates {(64, 29) (100, 22) (128, 16) (256, 15) (512, 11) (666, 15)  (1024, 34) (1758, 12)};
			\addplot [fill=yellow]  coordinates {(64, 17) (100, 10) (128, 7)  (256, 20) (512, 28)			 (1024, 23) (1758, 25)};


			\legend{Cluster 1, Cluster 2, Cluster 3, Cluster 4, CLuster 5, Cluster 6}
			\end{axis}
		\end{tikzpicture}
		\caption{Xmeans, K=6}
		\end{figure}

		\begin{figure}[ht!]
		%\centering
		\begin{tikzpicture}
			\begin{axis}[
			 	title={Proporciones 8 clústeres},
				ybar,
				symbolic x coords={64,100,128,256,512,666,1024,1758},
				xtick=data,
				ylabel={Porcentaje},
				bar width=4,
				ymax=30,
				enlargelimits=0.07,
				legend style={at={(0.5,-0.15)},
				anchor=north,legend columns=-1},
				ymajorgrids = true,
				enlarge x limits={abs=1cm},
				x tick label style={rotate=40, anchor=east, align=right,text width=1.5cm},
				ylabel near ticks,
				nodes near coords,
				width = 17cm,
				height = 8cm
			]
			\addplot [fill=gray!25] coordinates {(64, 19) (100, 7)  (128, 19) (256, 10) (512, 13) (666, 19) (1024, 9)  (1758, 14)};
			\addplot [fill=blue] 	coordinates {(64, 25) (100, 7)  (128, 9)  (256, 7) 	(512, 8)  (666, 10) (1024, 13) (1758, 10)};
			\addplot [fill=red] 	coordinates {(64, 12) (100, 27) (128, 14) (256, 18) (512, 5)  (666, 23) (1024, 10) (1758, 7)};
			\addplot [fill=green] 	coordinates {(64, 6)  (100, 12) (128, 12) (256, 19) (512, 9)  (666, 15) (1024, 7)  (1758, 12)};
			\addplot [fill=white] 	coordinates {(64, 10) (100, 19) (128, 7)  (256, 9) 	(512, 10) (666, 10) (1024, 27) (1758, 11)};
			\addplot [fill=yellow] 	coordinates {(64, 11) (100, 10) (128, 7)  (256, 11) (512, 17) (666, 14) (1024, 16) (1758, 25)};
			\addplot [fill=purple] 	coordinates {(64, 7)  (100, 8)  (128, 14) (256, 8) 	(512, 12) (666, 8)  (1024, 4)  (1758, 17)};
			\addplot [fill=black]	coordinates {(64, 11) (100, 11) (128, 19) (256, 19) (512, 26) 			(1024, 13) (1758, 4)};


			\legend{Cluster 1, Cluster 2, Cluster 3, Cluster 4, CLuster 5, Cluster 6, Cluster 7, Cluster 8}
			\end{axis}
		\end{tikzpicture}
		\caption{Xmeans, K=8}
		\end{figure}

		%\begin{itemize}
			%\item \textbf{4\_64: } Se han generado 4 clusters de proporciones: 34\%, 24\%, 18\%, 24\%.
			%\item \textbf{4\_100: } Se han generado 4 clusters de proporciones: 21\%, 14\%, 29\%, 36\%.
			%\item \textbf{4\_128: } Se han generado 4 clusters de proporciones: 34\%, 12\%, 38\%, 16\%.
			%\item \textbf{4\_256: } Se han generado 4 clusters de proporciones: 10\%, 17\%, 44\%, 30\%.
			%\item \textbf{4\_512: } Se han generado 4 clusters de proporciones: 31\%, 28\%, 24\%, 16\%.
			%\item \textbf{4\_666: } Se han generado 4 clusters de proporciones: 35\%, 47\%, 10\%, 7\%.
			%\item \textbf{4\_1024: } Se han generado 4 clusters de proporciones: 35\%, 47\%, 10\%, 7\%.
			%\item \textbf{4\_1758: } Se han generado 4 clusters de proporciones: 26\%, 36\%, 15\%, 22\%.
			%\item \textbf{6\_64: } Se han generado 6 clusters de proporciones: 7\%, 26\%, 15\%, 6\%, 29\%, 17\%.
			%\item \textbf{6\_100: } Se han generado 6 clusters de proporciones: 14\%, 12\%, 27\%, 14\%, 22\%, 10\%.
			%\item \textbf{6\_128: } Se han generado 6 clusters de proporciones: 27\%, 12\%, 26\%, 12\%, 16\%, 7\%.
			%\item \textbf{6\_256: } Se han generado 6 clusters de proporciones: 10\%, 8\%, 21\%, 26\%, 15\%, 20\%.
			%\item \textbf{6\_512: } Se han generado 6 clusters de proporciones: 25\%, 16\%, 5\%, 15\%, 11\%, 28\%.
			%\item \textbf{6\_666: } Se han generado 5 clusters de proporciones: 28\%, 7\%, 25\%, 26\%, 15\%.
			%\item \textbf{6\_1024: } Se han generado 6 clusters de proporciones: 15\%, 11\%, 10\%, 7\%, 34\%, 23\%.
			%\item \textbf{6\_1758: } Se han generado 6 clusters de proporciones: 23\%, 10\%, 8\%, 21\%, 12\%, 25\%.
			%\item \textbf{8\_64: } Se han generado 8 clusters de proporciones: 19\%, 25\%, 12\%, 6\%, 10\%,11\%, 7\%, 11\%.
			%\item \textbf{8\_100: } Se han generado 8 clusters de proporciones: 7\%, 7\%, 27\%, 12\%, 19\%, 10\%, 8\%, 11\%.
			%\item \textbf{8\_128: } Se han generado 8 clusters de proporciones: 19\%, 9\%, 14\%, 12\%, 7\%, 7\%, 14\%, 19\%.
			%\item \textbf{8\_256: } Se han generado 8 clusters de proporciones: 10\%, 7\%, 18\%, 19\%, 9\%, 11\%, 8\%, 19\%.
			%\item \textbf{8\_512: } Se han generado 8 clusters de proporciones: 13\%, 8\%, 5\%, 9\%, 10\%, 17\%, 12\%, 26\%.
			%\item \textbf{8\_666: } Se han generado 7 clusters de proporciones: 19\%, 10\%, 23\%, 15\%, 10\%, 14\%, 8\%.
			%\item \textbf{8\_1024: } Se han generado 8 clusters de proporciones: 9\%, 13\%, 10\%, 7\%, 27\%, 16\%, 4\%, 13\%.
			%\item \textbf{8\_1758: } Se han generado 8 clusters de proporciones: 14\%, 10\%, 7\%, 12\%, 11\%, 25\%, 17\%, 4\%.
		%\end{itemize}

		Como se puede observar el caso más estable es el caso de 4 clústeres. 6 y 8 clústeres varían mucho sus proporciones e incluso llegan a tener un clúster menos.


	\subsection{\textit{simplekmeans}}
		Nombraremos cada prueba con el formato `Nºcluster\_semilla', fijaremos el maxIterations a 500 y dejaremos el resto de parámetros por defecto. Los resultados de estas pruebas se pueden encontrar en la ruta «cluster/simplekmeans/buffer» y «cluster/simplekmeans/models».

		\begin{figure}[ht!]
		\centering
		\begin{tikzpicture}
			\begin{axis}[
			 	title={Proporciones 4 clústeres},
				ybar,
				symbolic x coords={64,100,128,256,512,666,1024,1758},
				xtick=data,
				ylabel={Porcentaje},
				ylabel near ticks,
				bar width=8,
				ymax=50,
				enlargelimits=0.07,
				legend style={at={(0.5,-0.15)},
				anchor=north,legend columns=-1},
				ymajorgrids = true,
				enlarge x limits={abs=1cm},
				x tick label style={rotate=40, anchor=east, align=right,text width=1.5cm},
				nodes near coords,
				width = 17cm,
				height = 8cm
			]
			\addplot [fill=gray!25] coordinates {(64, 34) (100, 10) (128, 24) (256, 28) (512, 25) (666, 39) (1024, 38) (1758, 39)};
			\addplot [fill=blue] coordinates {(64, 44) (100, 31) (128, 16) (256, 24) (512, 34) (666, 28) (1024, 29) (1758, 17)};
			\addplot [fill=red] coordinates {(64, 15) (100, 40) (128, 31) (256, 36) (512, 19) (666, 18) (1024, 18) (1758, 35)};
			\addplot [fill=green] coordinates {(64, 7) (100, 19) (128, 28) (256, 11) (512, 22) (666, 14) (1024, 16) (1758, 8)};


			\legend{Cluster 1, Cluster 2, Cluster 3, Cluster 4}
			\end{axis}
		\end{tikzpicture}
		\caption{SimpleKmeans, K=4}
		\end{figure}

		\begin{figure}[ht!]
		\centering
		\begin{tikzpicture}
			\begin{axis}[
			 	title={Proporciones 6 clústeres},
				ybar,
				symbolic x coords={64,100,128,256,512,666,1024,1758},
				xtick=data,
				ylabel={Porcentaje},
				ylabel near ticks,
				bar width=5,
				ymax=40,
				enlargelimits=0.07,
				legend style={at={(0.5,-0.15)},
				anchor=north,legend columns=-1},
				ymajorgrids = true,
				enlarge x limits={abs=1cm},
				x tick label style={rotate=40, anchor=east, align=right,text width=1.5cm},
				nodes near coords,
				width = 17cm,
				height = 8cm
			]
			\addplot [fill=gray!25] coordinates {(64, 22) (100, 10) (128, 11) (256, 10) (512, 17) (666, 19) (1024, 26) (1758, 21)};
			\addplot [fill=blue] coordinates {(64, 31) (100, 19) (128, 10) (256, 14) (512, 34) (666, 10) (1024, 10) (1758, 9)};
			\addplot [fill=red] coordinates {(64, 15) (100, 32) (128, 31) (256, 7) (512, 18) (666, 25) (1024, 15) (1758, 26)};
			\addplot [fill=green] coordinates {(64, 7) (100, 13) (128, 22) (256, 14) (512, 7) (666, 12) (1024, 8) (1758, 7)};
			\addplot [fill=white] coordinates {(64, 10) (100, 7) (128, 13) (256, 19) (512, 14) (666, 27) (1024, 21) (1758, 14)};
			\addplot [fill=yellow] coordinates {(64, 14) (100, 20) (128, 13) (256, 35) (512, 10) (666, 7) (1024, 19) (1758, 23)};


			\legend{Cluster 1, Cluster 2, Cluster 3, Cluster 4, CLuster 5, Cluster 6}
			\end{axis}
		\end{tikzpicture}
		\caption{SimpleKmeans, K=6}
		\end{figure}

		\begin{figure}[ht!]
		%\centering
		\begin{tikzpicture}
			\begin{axis}[
			 	title={Proporciones 8 clústeres},
				ybar,
				symbolic x coords={64,100,128,256,512,666,1024,1758},
				xtick=data,
				ylabel={Porcentaje},
				bar width=4,
				ymax=40,
				enlargelimits=0.07,
				legend style={at={(0.5,-0.15)},
				anchor=north,legend columns=-1},
				ymajorgrids = true,
				enlarge x limits={abs=1cm},
				x tick label style={rotate=40, anchor=east, align=right,text width=1.5cm},
				ylabel near ticks,
				nodes near coords,
				width = 17cm,
				height = 8cm
			]
			\addplot [fill=gray!25] coordinates {(64, 14) (100, 10) (128, 4)  (256, 10) (512, 18) (666, 10)  (1024, 19) (1758, 9)};
			\addplot [fill=blue] coordinates {(64, 13) (100, 8)  (128, 10) (256, 13) (512, 25) (666, 9)   (1024, 10) (1758, 2)};
			\addplot [fill=red] coordinates {(64, 14) (100, 24) (128, 31) (256, 7)  (512, 11) (666, 16)  (1024, 8)  (1758, 18)};
			\addplot [fill=green] coordinates {(64, 4)  (100, 12) (128, 22) (256, 11) (512, 5)  (666, 9)   (1024, 8)  (1758, 6)};
			\addplot [fill=white] coordinates {(64, 10) (100, 7)  (128, 9)  (256, 19) (512, 14) (666, 10)  (1024, 19) (1758, 12)};
			\addplot [fill=yellow] coordinates {(64, 9)  (100, 13) (128, 13) (256, 25) (512, 10) (666, 7)   (1024, 19) (1758, 25)};
			\addplot [fill=purple] coordinates {(64, 3)  (100, 16) (128, 4)  (256, 11) (512, 7)  (666, 25)  (1024, 8)  (1758, 16)};
			\addplot [fill=black] coordinates {(64, 33) (100, 12) (128, 7)  (256, 4)  (512, 10) (666, 14)  (1024, 9)  (1758, 12)};


			\legend{Cluster 1, Cluster 2, Cluster 3, Cluster 4, CLuster 5, Cluster 6, Cluster 7, Cluster 8}
			\end{axis}
		\end{tikzpicture}
		\caption{SimpleKmeans, K=8}
		\end{figure}

		%\begin{itemize}
			%\item \textbf{4\_64: } Se han generado 4 clusters de proporciones: 34\%, 44\%, 15\%, 7\%.
			%\item \textbf{4\_100: } Se han generado 4 clusters de proporciones: 10\%, 31\%, 40\%, 19\%.
			%\item \textbf{4\_128: } Se han generado 4 clusters de proporciones: 24\%, 16\%, 31\%, 28\%.
			%\item \textbf{4\_256: } Se han generado 4 clusters de proporciones: 28\%, 24\%, 36\%, 11\%.
			%\item \textbf{4\_512: } Se han generado 4 clusters de proporciones: 25\%, 34\%, 19\%, 22\%.
			%\item \textbf{4\_666: } Se han generado 4 clusters de proporciones: 39\%, 28\%, 18\%, 14\%.
			%\item \textbf{4\_1024: } Se han generado 4 clusters de proporciones: 38\%, 29\%, 18\%, 16\%.
			%\item \textbf{4\_1758: } Se han generado 4 clusters de proporciones: 39\%, 17\%, 35\%, 8\%.
			%\item \textbf{6\_64: } Se han generado 6 clusters de proporciones: 22\%, 31\%, 15\%, 7\%, 10\%, 14\%.
			%\item \textbf{6\_100: } Se han generado 6 clusters de proporciones: 10\%, 19\%, 32\%, 13\%, 7\%, 20\%.
			%\item \textbf{6\_128: } Se han generado 6 clusters de proporciones: 11\%, 10\%, 31\%, 22\%, 13\%, 13\%.
			%\item \textbf{6\_256: } Se han generado 6 clusters de proporciones: 10\%, 14\%, 7\%, 14\%, 19\%, 35\%.
			%\item \textbf{6\_512: } Se han generado 6 clusters de proporciones: 17\%, 34\%, 18\%, 7\%, 14\%, 10\%.
			%\item \textbf{6\_666: } Se han generado 6 clusters de proporciones: 19\%, 10\%, 25\%, 12\%, 27\%, 7\%.
			%\item \textbf{6\_1024: } Se han generado 6 clusters de proporciones: 26\%, 10\%, 15\%, 8\%, 21\%, 19\%.
			%\item \textbf{6\_1758: } Se han generado 6 clusters de proporciones: 21\%, 9\%, 26\%, 7\%, 14\%, 23\%.
			%\item \textbf{8\_64: } Se han generado 8 clusters de proporciones: 14\%, 13\%, 14\%, 4\%, 10\%, 9\%, 3\%, 33\%.
			%\item \textbf{8\_100: } Se han generado 8 clusters de proporciones: 10\%, 8\%, 24\%, 12\%, 7\%, 13\%, 16\%, 12\%.
			%\item \textbf{8\_128: } Se han generado 8 clusters de proporciones: 4\%, 10\%, 31\%, 22\%, 9\%, 13\%, 4\%, 7\%.
			%\item \textbf{8\_256: } Se han generado 8 clusters de proporciones: 10\%, 13\%, 7\%, 11\%, 19\%, 25\%, 11\%, 4\%.
			%\item \textbf{8\_512: } Se han generado 8 clusters de proporciones: 18\%, 25\%, 11\%, 5\%, 14\%, 10\%, 7\%, 10\%.
			%\item \textbf{8\_666: } Se han generado 8 clusters de proporciones: 10\%, 9\%, 16\%, 9\%, 10\%, 7\%, 25\%, 14\%.
			%\item \textbf{8\_1024: } Se han generado 8 clusters de proporciones: 19\%, 10\%, 8\%, 8\%, 19\%, 19\%, 8\%, 9\%.
			%\item \textbf{8\_1758: } Se han generado 8 clusters de proporciones: 9\%, 2\%, 18\%, 6\%, 12\%, 25\%, 16\%, 12\%.
		%\end{itemize}

		En este caso, se puede observar que el cluster 4 es el más estable. A diferencia de 6 y 8 clústeres en los cuales las proporciones varían notablemente

	\subsection{Conclusión}

	HBDIASDIABDASBDIASBDIABDIABSIDBASIHIUHWEI
	
	Ningún cluster es realmente estable, todos tienen variaciones de más del 10\%, sin embargo dentro de esas variaciones, 4 clústeres son lo que tienen unas variaciones de menor nivel, por lo que procederemos a realizar una implementación de 4 clústeres. 

	Xmeans y SimpleKMeans poseen resultados muy similares, sin embargo la variación de SimpleKMeans es menor que la de Xmeans, por lo que elegiremos SimpleKMeans.

	De los clústeres posibles generados se utilizará el modelo generado con semilla = 128 debido a que es el más equilibrado de todos los modelos.

\section{Implementación del agente}
	Para implementar la clusterización en nuestro agente hemos creado un nuevo agente llamado \textit{clusterAgent}. Este agente posee cuatro listas que representan los cuatro clústeres que hemos seleccionado.\\
	Cada instancia del cluster se representa a su vez mediante una lista de los atributos seleccionados. En la posición 0 de cada clúster se localiza su centroide.

	Para crear los clústeres se lee del fichero \textit{instanciasClusterizadas.arff} que se localiza en la carpeta «pacman», se evalúa a que clúster pertenece y se añade al mismo. Una vez se tienen generados todos los clústeres es posible evaluar a que clúster pertenece la instancia actual.\\
Para realizar la función de pertenencia se calcula la distancia euclídea entre el centroide de cada clúster y la distancia actual, de forma que se obtienen cuatro distancias. De estas distancias se devuelve un string que indica a que clúster tiene menor distancia la instancia actual.\\
Una vez hemos calculado el clúster hay que realizar la función de similitud. Para ello se calculará la distancia euclídea a cada instancia del clúster y se guardará el valor de su función. Cada vez que se encuentra una similitud mayor con una instancia, es decir, la distancia euclídea es menor que en una comparación anterior, se comprobará que el movimiento de esa instancia pertenece al conjunto de movimientos legales en la instancia actual, ya que es posible que la instancia similar tenga como movimiento un movimiento que no es legal en esta situación. Si el movimiento es legal y el valor de la función es menor o igual al valor previo se tomará esta nueva instancia como la más similar, de no ser legal, se ignorará.


\section{Fase de Evaluación}
	JUSTIFICAR RESULTADOS OBTENIDOS

\section{Preguntas}
	\subsection{¿Por qué ha sido útil realizar clustering previa de las instancias?}
	Al tener un conjunto muy grande de instancias, buscar una similar dentro del conjunto global podría ser muy costoso en tiempo. Si dividimos el conjunto total en una serie de subconjuntos (clústers) en los que agrupamos instancias similares se reduce considerablemente el tiempo de búsqueda dentro del conjunto total.

	\subsection{¿Por qué es importante usar pocos atributos en técnicas de aprendizaje no supervisado?}
	Al dividir el conjunto total en otros más pequeños se realiza en función de similitudes entre las instancias, si poseemos una gran cantidad de atributos y debido a eso, probablemente muchos que no sean importantes, estaremos empeorando el algoritmo de clusterización. Esto se debe porque le obligamos a calcular similitudes teniendo en cuenta esos atributos que no aportan información real.

	\subsection{¿Qué ventaja tiene el uso del aprendizaje basado en instancias con respecto al visto en la práctica 1?}
	AHORA MISMO NO SE ME OCURRE NADA

	\subsection{¿Consideras que el agente funcionaría mejor si se introdujesen más ejemplos? ¿Por qué?}
	Puede que si, puede que no. Debido a que el agente se basa en instancias similares que tenemos almacenadas, probablemente acierte en un mayor número de ocasiones la acción a realizar. O quizás no, debido a que las nuevas instancias que hemos introducido son repeticiones en momentos distintos de situaciones ya contempladas con ejemplos previos.

\section{Conclusiones}
			
\end{document}
