# bustersAgents.py
# ----------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import util
import os.path
import pprint
import Queue
from game import Agent
from game import Directions
from keyboardAgents import KeyboardAgent
import inference
import busters
from pprint import pprint
import math

#Necesario para la funcion de evaluacion del agente
from time import time

class NullGraphics:
    "Placeholder for graphics"
    def initialize(self, state, isBlue = False):
        pass
    def update(self, state):
        pass
    def pause(self):
        pass
    def draw(self, state):
        pass
    def updateDistributions(self, dist):
        pass
    def finish(self):
        pass

class KeyboardInference(inference.InferenceModule):
    """
    Basic inference module for use with the keyboard.
    """
    def initializeUniformly(self, gameState):
        "Begin with a uniform distribution over ghost positions."
        self.beliefs = util.Counter()
        for p in self.legalPositions: self.beliefs[p] = 1.0
        self.beliefs.normalize()

    def observe(self, observation, gameState):
        noisyDistance = observation
        emissionModel = busters.getObservationDistribution(noisyDistance)
        pacmanPosition = gameState.getPacmanPosition()
        allPossible = util.Counter()
        for p in self.legalPositions:
            trueDistance = util.manhattanDistance(p, pacmanPosition)
            if (trueDistance != None ) and (emissionModel[trueDistance] > 0):
                allPossible[p] = emissionModel[trueDistance]
        allPossible.normalize()
        self.beliefs = allPossible

    def elapseTime(self, gameState):
        pass

    def getBeliefDistribution(self):
        return self.beliefs


class BustersAgent:

    turno = 0
    colaFuturo = Queue.Queue()
    colaMinima = Queue.Queue()

    #Atributos para la funcion de evaluacion del agente
    #Distancia recorrida
    recorrido = 0
    #Momento en el que se inicio el programa
    reloj = time()

    "An agent that tracks and displays its beliefs about ghost positions."

    def __init__( self, index = 0, inference = "ExactInference", ghostAgents = None, observeEnable = True, elapseTimeEnable = True):
        inferenceType = util.lookup(inference, globals())
        self.inferenceModules = [inferenceType(a) for a in ghostAgents]
        self.observeEnable = observeEnable
        self.elapseTimeEnable = elapseTimeEnable


    def registerInitialState(self, gameState):
        "Initializes beliefs and inference modules"
        import __main__
        self.display = __main__._display
        for inference in self.inferenceModules:
            inference.initialize(gameState)
        self.ghostBeliefs = [inf.getBeliefDistribution() for inf in self.inferenceModules]
        self.firstMove = True

    def observationFunction(self, gameState):
        "Removes the ghost states from the gameState"
        #agents = gameState.data.agentStates
        #gameState.data.agentStates = [agents[0]] + [None for i in range(1, len(agents))]
        return gameState

    def getAction(self, gameState):
        "Updates beliefs, then chooses an action based on updated beliefs."
        for index, inf in enumerate(self.inferenceModules):
            if not self.firstMove and self.elapseTimeEnable:
                inf.elapseTime(gameState)
            self.firstMove = False
            if self.observeEnable:
                inf.observeState(gameState)
            self.ghostBeliefs[index] = inf.getBeliefDistribution()
        self.display.updateDistributions(self.ghostBeliefs)
        return self.chooseAction(gameState)

    def chooseAction(self, gameState):
        "By default, a BustersAgent just stops.  This should be overridden."
        return Directions.STOP

    #Codigo para guardar los datos. Al ponerlo en esta clase el resto de agentes heredan las funciones.
    def saveData(self,gameState, nombre='default'):
        if (os.path.isfile(nombre+'.arff') != True):
            self.crearEncabezado(nombre)
        datos = ''
        for distancia in gameState.getNoisyGhostDistances():
            if (distancia == None):
                datos += '-1,'
            else:
                datos += str(distancia)+','
        
        datos += str(gameState.data.agentStates[0].getPosition()[0])+','
        datos += str(gameState.data.agentStates[0].getPosition()[1])+','
        datos += str(gameState.data.score)+',' #Score now

        if Directions.NORTH in gameState.getLegalActions(0):
            datos += str(1)+','
        else: 
            datos += str(0)+','
        if Directions.SOUTH in gameState.getLegalActions(0):
            datos += str(1)+','
        else: 
            datos += str(0)+','
        if Directions.WEST in gameState.getLegalActions(0):
            datos += str(1)+','
        else: 
            datos += str(0)+','
        if Directions.EAST in gameState.getLegalActions(0):
            datos += str(1)+','
        else: 
            datos += str(0)+','

        minAct = self.funcion1(gameState)
        self.colaMinima.put(minAct)
        datos += gameState.data.agentStates[0].getDirection() + ','

        self.colaFuturo.put(datos)
        #A partir del turno 2 siempre se desencola
        if self.turno >= 2:
            datos = self.colaFuturo.get()
            for distancia in gameState.getNoisyGhostDistances():
                if (distancia == None):
                    datos += '-1,'
                else:
                    datos += str(distancia)+','
        
            datos += str(gameState.data.agentStates[0].getPosition()[0])+','
            datos += str(gameState.data.agentStates[0].getPosition()[1])+','
            datos += str(gameState.data.score)+','
            
            if Directions.NORTH in gameState.getLegalActions(0):
                datos += str(1)+','
            else: 
                datos += str(0)+','
            if Directions.SOUTH in gameState.getLegalActions(0):
                datos += str(1)+','
            else: 
                datos += str(0)+','
            if Directions.WEST in gameState.getLegalActions(0):
                datos += str(1)+','
            else: 
                datos += str(0)+','
            if Directions.EAST in gameState.getLegalActions(0):
                datos += str(1)+','
            else: 
                datos += str(0)+','
            minimaFuturo = self.funcion1(gameState)
            evaluacion = self.colaMinima.get() - minimaFuturo

            if evaluacion > -5:
                datos += str(evaluacion)
                fileHandle = open(nombre +'.arff','a')
                fileHandle.write(datos + '\n')
                fileHandle.close()

        self.turno += 1

    #Funcion para crear el encabezado si el fichero no existia
    def crearEncabezado(self,nombre):
        fileHandle = open(nombre+'.arff','a')
        #relation
        datos = '@relation '+ nombre + '\n\n'
        #atributos
        #   distancia fantasma Azul
        datos += '@attribute lightYearsAzul numeric\n'
        #   distancia fantasma Naranja
        datos += '@attribute lightYearsNaranja numeric\n'
        #   distancia fantasma Cian
        datos += '@attribute lightYearsCian numeric\n'
        #   distancia fantasma Mandarina
        datos += '@attribute lightYearsMandarina numeric\n'
        #   posicion X pacman
        datos += '@attribute patrullaX numeric\n'
        #   posicion Y pacman
        datos += '@attribute patrullaY numeric\n'
        #   scoreActual
        datos += '@attribute scoreNow numeric\n'
        #   Si es legal moverse arriba (0=no, 1=si)
        datos += '@attribute legalUp numeric\n'
        #   Si es legal moverse abajo (0=no, 1=si)
        datos += '@attribute legalDown numeric\n'
        #   Si es legal moverse izq (0=no, 1=si)
        datos += '@attribute legalLenin numeric\n'
        #   Si es legal moverse drch (0=no, 1=si)
        datos += '@attribute legalFranco numeric\n'
        #   direccion (clase) del pacman
        datos += '@attribute fate {North, West, East, South, Stop}\n'
        #   distancia futura fantasma Azul
        datos += '@attribute hyperspaceAzul numeric\n'
        #   distancia futura fantasma Naranja
        datos += '@attribute hyperspaceNaranja numeric\n'
        #   distancia futura fantasma Cian
        datos += '@attribute hyperspaceCian numeric\n'
        #   distancia futura fantasma Mandarina
        datos += '@attribute hyperspaceMandarina numeric\n'
        #   posicion futura X pacman
        datos += '@attribute futuroX numeric\n'
        #   posicion futura Y pacman
        datos += '@attribute futuroY numeric\n'
        #   scoreFuturo
        datos += '@attribute scoreFuturo numeric\n'
        #   Si es legal moverse arriba en el futuro (0=no, 1=si)
        datos += '@attribute legalSuperUp numeric\n'
        #   Si es legal moverse abajo en el futuro (0=no, 1=si)
        datos += '@attribute legalSuperDown numeric\n'
        #   Si es legal moverse izq en el futuro (0=no, 1=si)
        datos += '@attribute legalPutin numeric\n'
        #   Si es legal moverse drch en el futuro (0=no, 1=si)
        datos += '@attribute legalTrump numeric\n'
        #   Calculo de la funcion de evaluacion
        datos += '@attribute funcion1 numeric\n\n'

        datos += '@data\n\n'

        fileHandle.write(datos + '\n')
        fileHandle.close()

    def funcion1(self,gameState):
        distMax = math.sqrt(math.pow(gameState.data.layout.width,2) + math.pow(gameState.data.layout.height,2))
        distMin = distMax
        for i in gameState.getNoisyGhostDistances():
            aux = i
            if (aux is not None) and (aux < distMin):
                distMin = aux
        if distMin is 0:
            distMin = 0.7

        return distMin

	
    #Funcion que sirve para guardar los datos que pide la evaluacion del agente.
    #Recibe por argumento el nombre del fichero y los datos a guardar
    def funcEvaluacion(self, nombre, distancia, fantasmas, tiempo):
        fileHandle = open(nombre +'.arff','a')
        fileHandle.write(str(distancia) + ',' + str(fantasmas) + ',' + str(tiempo) + '\n')
        fileHandle.close()

class BustersKeyboardAgent(BustersAgent, KeyboardAgent):
    "An agent controlled by the keyboard that displays beliefs about ghost positions."


    def __init__(self, index = 0, inference = "KeyboardInference", ghostAgents = None):
        KeyboardAgent.__init__(self, index)
        BustersAgent.__init__(self, index, inference, ghostAgents)

    def getAction(self, gameState):
        return BustersAgent.getAction(self, gameState)

    def chooseAction(self, gameState):
        self.saveData(gameState, 'instanciasTeclado')
        ################## EVALUACION DEL AGENTE
        #Numero de fantasmas muertos
        muertos = 0
        for i in gameState.getNoisyGhostDistances():
            if (i == None):
                muertos += 1

        if KeyboardAgent.getAction(self, gameState) != Directions.STOP:
            self.recorrido += 1
        #Tiempo que lleva en ejecucion el programa
        tiempo = time() - self.reloj
        self.funcEvaluacion('evaluacionTeclado', self.recorrido, muertos, tiempo)

        #########################################


        return KeyboardAgent.getAction(self, gameState)


from distanceCalculator import Distancer
from game import Actions
from game import Directions
import random, sys

'''Random PacMan Agent'''
class RandomPAgent(BustersAgent):

    def registerInitialState(self, gameState):
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)
        
    ''' Example of counting something'''
    def countFood(self, gameState):
        food = 0
        for width in gameState.data.food:
            for height in width:
                if(height == True):
                    food = food + 1
        return food

    ''' Print the layout'''  
    def printGrid(self, gameState):
        table = ""
        ##print(gameState.data.layout) ## Print by terminal
        for x in range(gameState.data.layout.width):
            for y in range(gameState.data.layout.height):
                food, walls = gameState.data.food, gameState.data.layout.walls
                table = table + gameState.data._foodWallStr(food[x][y], walls[x][y]) + ","
        table = table[:-1]
        return table

    def printLineData(self,gameState):

        '''Observations of the state
        
        print(str(gameState.livingGhosts))
        print(gameState.data.agentStates[0])
        print(gameState.getNumFood())
        print (gameState.getCapsules())
        width, height = gameState.data.layout.width, gameState.data.layout.height
        print(width, height)
        print(gameState.getNoisyGhostDistances())
        print(gameState.data.layout)'''
      
        '''END Observations of the state'''
        
        print gameState

        self.saveData(gameState, 'training_tutorial1')

        weka_line = ""
        for i in gameState.livingGhosts:
            weka_line = weka_line + str(i) + ","
        weka_line = weka_line + str(gameState.getNumFood()) + "," 
        for i in gameState.getCapsules():
            weka_line = weka_line + str(i[0]) + "," + str(i[1]) + ","
        for i in gameState.getNoisyGhostDistances():
            weka_line = weka_line + str(i) + ","
        weka_line = weka_line + str(gameState.data.score) + "," +\
        str(len(gameState.data.capsules))  + "," + str(self.countFood(gameState)) +\
        "," + str(gameState.data.agentStates[0].configuration.pos[0]) + "," +\
        str(gameState.data.agentStates[0].configuration.pos[0])  +\
        "," + str(gameState.data.agentStates[0].scaredTimer) + "," +\
        self.printGrid(gameState) + "," +\
        str(gameState.data.agentStates[0].numReturned) + "," +\
        str(gameState.data.agentStates[0].getPosition()[0]) + "," +\
        str(gameState.data.agentStates[0].getPosition()[1])+ "," +\
        str(gameState.data.agentStates[0].numCarrying)+ "," +\
        str(gameState.data.agentStates[0].getDirection())
        print(weka_line)
        
        
    def chooseAction(self, gameState):
        move = Directions.STOP
        legal = gameState.getLegalActions(0) ##Legal position from the pacman
        move_random = random.randint(0, 3)
        self.printLineData(gameState)
        if   ( move_random == 0 ) and Directions.WEST in legal:  move = Directions.WEST
        if   ( move_random == 1 ) and Directions.EAST in legal: move = Directions.EAST
        if   ( move_random == 2 ) and Directions.NORTH in legal:   move = Directions.NORTH
        if   ( move_random == 3 ) and Directions.SOUTH in legal: move = Directions.SOUTH
        return move
        
class GreedyBustersAgent(BustersAgent):
    "An agent that charges the closest ghost."

    #variables necesarias para poder decidir la nueva direccion
    oldDistance = -1
    newDistance = 0
    oldDirection = None
    presa = 0

    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)

    def chooseAction(self, gameState):

        pacmanPosition = gameState.getPacmanPosition()
        legal = [a for a in gameState.getLegalPacmanActions()]
        livingGhosts = gameState.getLivingGhosts()
        livingGhostPositionDistributions = \
            [beliefs for i, beliefs in enumerate(self.ghostBeliefs)
             if livingGhosts[i+1]]

         #Es necesario presa+1 ya que getLivingGhosts incluye a Pac-Man
        while (gameState.getLivingGhosts()[self.presa+1]==False):
            self.presa += 1

        #Se obtiene la nueva distancia al fantasma
        self.newDistance = gameState.getNoisyGhostDistances()[self.presa]

        #si la nueva distancia es mas cercana o igual al fantasma, sigue la direccion
        if (self.oldDistance >= self.newDistance):
            self.oldDistance = self.newDistance
            legalMoves = gameState.getLegalActions(0)
            if (legalMoves.count(self.oldDirection) == 0):
                self.oldDirection = Directions.STOP                
            self.saveData(gameState,'instanciasGreedy')
            return self.oldDirection
        else:
            #En caso de no acercarse, si no, alejarse, hay que cambiar direccion
            legalMoves = gameState.getLegalActions(0)
            move_random = random.randint(0, 3)
            #Se evita que la nueva direccion sea la misma que llevaba antes
            if(move_random == 0) and (Directions.WEST in legalMoves) and (Directions.WEST != self.oldDirection):
                self.oldDirection = Directions.WEST
            elif(move_random == 1) and (Directions.EAST in legalMoves) and (Directions.EAST != self.oldDirection):
                self.oldDirection = Directions.EAST
            elif(move_random == 2) and (Directions.NORTH in legalMoves) and (Directions.NORTH != self.oldDirection):
                self.oldDirection = Directions.NORTH
            elif(move_random == 3) and (Directions.SOUTH in legalMoves) and (Directions.SOUTH != self.oldDirection):
                self.oldDirection = Directions.SOUTH
            else:
                #Si no se genera ningun movimiento legal, se queda parado
                self.oldDirection = Directions.STOP
            self.oldDistance = gameState.getNoisyGhostDistances()[self.presa]
            self.saveData(gameState,'instanciasGreedy')
            return self.oldDirection

#Agente que va a por el fantasma mas cercano
class CloserBustersAgent(BustersAgent):
    "An agent that charges the closest ghost."

    #variables necesarias para poder decidir la nueva direccion
    oldDistance = -1
    newDistance = 0
    oldDirection = None

    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)

    def chooseAction(self, gameState):

        #Se evaluan todas las distancias y se escoge la mas cercana
        distancias = gameState.getNoisyGhostDistances()
        cercana = gameState.data.layout.width * gameState.data.layout.height
        for i in distancias:
            if (i != None) and (i <= cercana):
                cercana = i

        #Se obtiene la nueva distancia al fantasma
        self.newDistance = cercana

        #si la nueva distancia es mas cercana o igual al fantasma, sigue la direccion
        if (self.oldDistance >= self.newDistance):
            self.oldDistance = self.newDistance
            legalMoves = gameState.getLegalActions(0)
            if (legalMoves.count(self.oldDirection) == 0):
                self.oldDirection = Directions.STOP                
            self.saveData(gameState, 'instanciasCloser')

            ################## EVALUACION DEL AGENTE

            muertos = 0
            for i in gameState.getNoisyGhostDistances():
                if (i == None):
                    muertos += 1

            if self.oldDirection != Directions.STOP:
                self.recorrido += 1

            #Tiempo que lleva en ejecucion el programa
            tiempo = time() - self.reloj
            self.funcEvaluacion('evaluacionCloser', self.recorrido, muertos, tiempo)

            #########################################

            return self.oldDirection
        else:
            #En caso de no acercarse, si no, alejarse, hay que cambiar direccion
            legalMoves = gameState.getLegalActions(0)
            move_random = random.randint(0, 3)
            #Se evita que la nueva direccion sea la misma que llevaba antes
            if (move_random == 0) and (Directions.WEST in legalMoves) and (Directions.WEST != self.oldDirection):
                self.oldDirection = Directions.WEST
            elif (move_random == 1) and (Directions.EAST in legalMoves) and (Directions.EAST != self.oldDirection):
                self.oldDirection = Directions.EAST
            elif (move_random == 2) and (Directions.NORTH in legalMoves) and (Directions.NORTH != self.oldDirection):
                self.oldDirection = Directions.NORTH
            elif (move_random == 3) and (Directions.SOUTH in legalMoves) and (Directions.SOUTH != self.oldDirection):
                self.oldDirection = Directions.SOUTH
            else:
                #Si no se genera ningun movimiento legal, se queda parado
                self.oldDirection = Directions.STOP
            self.oldDistance = cercana
            self.saveData(gameState, 'instanciasCloser')

            ################## EVALUACION DEL AGENTE

            muertos = 0
            for i in gameState.getNoisyGhostDistances():
                if (i == None):
                    muertos += 1

            if self.oldDirection != Directions.STOP:
                self.recorrido += 1

            #Tiempo que lleva en ejecucion el programa
            tiempo = time() - self.reloj
            self.funcEvaluacion('evaluacionCloser', self.recorrido, muertos, tiempo)

            #########################################

            return self.oldDirection

#Agente que va a por el fantasma mas cercano basado en el arbol creado por weka
class j48Agent(BustersAgent):

    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)

    def chooseAction(self, gameState):

        #Movimiento a devolver
        move = None

        #Se recuperan los movimientos legales ya que hay que evitar
        #movimientos indebido como atravesar pareces
        legalMoves = gameState.getLegalActions(0)

        #Se genera el arbol de J48

        patrullaX = gameState.data.agentStates[0].getPosition()[0]
        patrullaY = gameState.data.agentStates[0].getPosition()[1]
        lightYears = gameState.getNoisyGhostDistances()
        livingLavidaLoca = gameState.livingGhosts
        if patrullaX <= 1:
            move = Directions.STOP
        else:
            if livingLavidaLoca[4] is True:
                if lightYears[1] <= 4:
                    if patrullaY <= 13:
                        if patrullaY <= 10:
                            if lightYears[3] <= 15:
                                if patrullaX <= 10:
                                    if livingLavidaLoca[1] is True: 
                                        if patrullaX <= 9:
                                            if patrullaY <= 6: move = Directions.SOUTH
                                            else:
                                                if patrullaX <= 7: move = Directions.SOUTH
                                                else: move = Directions.EAST
                                        else:
                                            if lightYears[3] <= 9: move = Directions.NORTH
                                            else: move = Directions.SOUTH
                                    else:
                                        if patrullaY <= 3: move = Directions.SOUTH
                                        else:
                                            if lightYears[3] <= 7: move = Directions.NORTH
                                            else: move = Directions.EAST
                                else:
                                    if lightYears[3] <= 2: move = Directions.SOUTH
                                    else: move = Directions.EAST
                            else: move = Directions.WEST
                        else: move = Directions.EAST
                    else:
                        if lightYears[0] <= 5: move = Directions.WEST
                        else: move = Directions.SOUTH
                else:
                    if patrullaX <= 14:
                        if patrullaY <= 10: move = Directions.STOP
                        else:
                            if patrullaY <= 11: move = Directions.EAST
                            else: move = Directions.NORTH
                    else:
                        if lightYears[1] <= 14:
                            if patrullaX <= 18:
                                if patrullaX <= 15: move = Directions.NORTH
                                else:
                                    if patrullaX <= 17: move = Directions.WEST
                                    else:
                                        if lightYears[1] <= 9: move = Directions.NORTH
                                        else: move = Directions.EAST
                            else: move = Directions.NORTH
                        else: move = Directions.EAST
            else:
                if lightYears[3] <= 0:
                    if patrullaY <= 8:
                        if patrullaY <= 5:
                            if lightYears[0] <= 9: move = Directions.WEST
                            else: move = Directions.SOUTH
                        else: move = Directions.SOUTH
                    else:
                        if livingLavidaLoca[2] is True:
                            if lightYears[1] <= 6: move = Directions.WEST
                            else: move = Directions.NORTH
                        else:
                            if patrullaX <= 11: move = Directions.WEST
                            else: move = Directions.EAST
                else: move = Directions.SOUTH
        #Para evitar acciones ilegales se elige un movimiento legal al azar
        #cuando no se valido el elegido por el arbol.
        if move not in legalMoves: 
            move = legalMoves[random.randint(0, len(legalMoves)-1)]
        self.saveData(gameState, 'instanciasJ48')

        ################## EVALUACION DEL AGENTE

        muertos = 0
        for i in gameState.getNoisyGhostDistances():
            if (i == None):
                muertos += 1
        if move != Directions.STOP:
            self.recorrido += 1

        #Tiempo que lleva en ejecucion el programa
        tiempo = time() - self.reloj
        self.funcEvaluacion('evaluacionJ48', self.recorrido, muertos, tiempo)

        #########################################

        return move

class clusterAgent(BustersAgent):

	
    #Se inicializan los clusteres con los centroides
    cluster1 = [[5.625,7.2431,6.8194,11.2847,9.4097,8.8333,0.8542,0.9514,0,0.7083]]
    cluster2 = [[20.1649,17.9072,11.268,2.299,19.196,13.6907,0.4021,0.9485,1,0.8557]]
    cluster3 = [[4.2419,5.8495,6.2204,12.4409,9.4892,90161,0,0.1935,0.9839,0.9892]]
    cluster4 = [[2.9586,2.6923,2.3254,9.2663,10.7811,7.0473,0.9586,0.6982,1,0.7219]]


    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)

        #Se inicializan los clusteres
        self.crearClusters()

    def chooseAction(self, gameState):


        move = self.funcSimilitud(gameState, self.funcPertenencia(gameState))

        self.saveData(gameState, 'movimientoCluster')

        ################## EVALUACION DEL AGENTE

        muertos = 0
        for i in gameState.getNoisyGhostDistances():
            if (i == None):
                muertos += 1
        if move != Directions.STOP:
            self.recorrido += 1

        #Tiempo que lleva en ejecucion el programa
        tiempo = time() - self.reloj
        self.funcEvaluacion('evaluacionCluster', self.recorrido, muertos, tiempo)

        #########################################

        return move


    def crearClusters(self):
        texto = open('instanciasClusterizadas.arff','r').read()
        lineas = texto.split('\n')
        for i in lineas:
            #se divide la linea por las comas. Son 12 elementos
            #por lo que la posicion 11 es el cluster al que pertenece
            fila = i.split(',')
            #no es necesario guardar ya el cluster
            if fila[12] == 'cluster1':
                self.cluster1.append(fila)
            if fila[12] == 'cluster2':
                self.cluster2.append(fila)
            if fila[12] == 'cluster3':
                self.cluster3.append(fila)
            if fila[12] == 'cluster4':
                self.cluster4.append(fila)

    def funcPertenencia(self,gameState):
        if (gameState.getNoisyGhostDistances()[0] != None):
            lightYearsAzul = gameState.getNoisyGhostDistances()[0]
        else:
            lightYearsAzul = -1
        if (gameState.getNoisyGhostDistances()[1] != None):
            lightYearsNaranja = gameState.getNoisyGhostDistances()[1]
        else:
            lightYearsNaranja = -1
        if (gameState.getNoisyGhostDistances()[2] != None):
            lightYearsCian = gameState.getNoisyGhostDistances()[2]
        else:
            lightYearsCian = -1
        if (gameState.getNoisyGhostDistances()[3] != None):
            lightYearsMandarina = gameState.getNoisyGhostDistances()[3]
        else:
            lightYearsMandarina = -1
        patrullaX = gameState.data.agentStates[0].getPosition()[0]
        patrullaY = gameState.data.agentStates[0].getPosition()[1]
        if Directions.NORTH in gameState.getLegalActions(0):
            legalUp = 1
        else: 
            legalUp = 0
        if Directions.SOUTH in gameState.getLegalActions(0):
            legalDown = 1
        else: 
            legalDown = 0
        if Directions.WEST in gameState.getLegalActions(0):
            legalLenin = 1
        else: 
            legalLenin = 0
        if Directions.EAST in gameState.getLegalActions(0):
            legalFranco = 1
        else: 
            legalFranco = 0

        #Distancia euclidea a cada centroide de los clusteres con el objetivo de conocer a que cluster pertenece
        distC1 = math.sqrt( pow(lightYearsAzul-self.cluster1[0][0], 2) + pow(lightYearsNaranja-self.cluster1[0][1], 2) + pow(lightYearsCian-self.cluster1[0][2], 2)
                    + pow(lightYearsMandarina-self.cluster1[0][3], 2) + pow(patrullaX-self.cluster1[0][4], 2) + pow(patrullaY-self.cluster1[0][5], 2)
                    + pow(legalUp-self.cluster1[0][6], 2) + pow(legalDown-self.cluster1[0][7], 2) + pow(legalLenin-self.cluster1[0][8], 2)
                    + pow(legalFranco-self.cluster1[0][9], 2) )

        distC2 = math.sqrt( pow(lightYearsAzul-self.cluster2[0][0], 2) + pow(lightYearsNaranja-self.cluster2[0][1], 2) + pow(lightYearsCian-self.cluster2[0][2], 2)
                    + pow(lightYearsMandarina-self.cluster2[0][3], 2) + pow(patrullaX-self.cluster2[0][4], 2) + pow(patrullaY-self.cluster2[0][5], 2)
                    + pow(legalUp-self.cluster2[0][6], 2) + pow(legalDown-self.cluster2[0][7], 2) + pow(legalLenin-self.cluster2[0][8], 2)
                    + pow(legalFranco-self.cluster2[0][9], 2) )

        distC3 = math.sqrt( pow(lightYearsAzul-self.cluster3[0][0], 2) + pow(lightYearsNaranja-self.cluster3[0][1], 2) + pow(lightYearsCian-self.cluster3[0][2], 2)
                    + pow(lightYearsMandarina-self.cluster3[0][3], 2) + pow(patrullaX-self.cluster3[0][4], 2) + pow(patrullaY-self.cluster3[0][5], 2)
                    + pow(legalUp-self.cluster3[0][6], 2) + pow(legalDown-self.cluster3[0][7], 2) + pow(legalLenin-self.cluster3[0][8], 2)
                    + pow(legalFranco-self.cluster3[0][9], 2) )

        distC4 = math.sqrt( pow(lightYearsAzul-self.cluster4[0][0], 2) + pow(lightYearsNaranja-self.cluster4[0][1], 2) + pow(lightYearsCian-self.cluster4[0][2], 2)
                    + pow(lightYearsMandarina-self.cluster4[0][3], 2) + pow(patrullaX-self.cluster4[0][4], 2) + pow(patrullaY-self.cluster4[0][5], 2)
                    + pow(legalUp-self.cluster4[0][6], 2) + pow(legalDown-self.cluster4[0][7], 2) + pow(legalLenin-self.cluster4[0][8], 2)
                    + pow(legalFranco-self.cluster4[0][9], 2) )

        #Se devuelve el cluster del centroide al que sea mas cercado
        if (distC1 < distC2) and (distC1 < distC3) and (distC1 < distC4):
            return 'cluster1'
        if (distC2 < distC1) and (distC2 < distC3) and (distC2 < distC4):
            return 'cluster2'
        if (distC3 < distC2) and (distC3 < distC1) and (distC3 < distC4):
            return 'cluster3'
        if (distC4 < distC2) and (distC4 < distC3) and (distC4 < distC1):
            return 'cluster4'

    def funcSimilitud(self,gameState, cluster):

        #Seleccionamos el cluster donde buscar
        if cluster == 'cluster1':
            clust = self.cluster1
        if cluster == 'cluster2':
            clust = self.cluster2
        if cluster == 'cluster3':
            clust = self.cluster3
        if cluster == 'cluster4':
            clust = self.cluster4

        if (gameState.getNoisyGhostDistances()[0] != None):
            lightYearsAzul = gameState.getNoisyGhostDistances()[0]
        else:
            lightYearsAzul = -1
        if (gameState.getNoisyGhostDistances()[1] != None):
            lightYearsNaranja = gameState.getNoisyGhostDistances()[1]
        else:
            lightYearsNaranja = -1
        if (gameState.getNoisyGhostDistances()[2] != None):
            lightYearsCian = gameState.getNoisyGhostDistances()[2]
        else:
            lightYearsCian = -1
        if (gameState.getNoisyGhostDistances()[3] != None):
            lightYearsMandarina = gameState.getNoisyGhostDistances()[3]
        else:
            lightYearsMandarina = -1
        patrullaX = gameState.data.agentStates[0].getPosition()[0]
        patrullaY = gameState.data.agentStates[0].getPosition()[1]
        if Directions.NORTH in gameState.getLegalActions(0):
            legalUp = 1
        else: 
            legalUp = 0
        if Directions.SOUTH in gameState.getLegalActions(0):
            legalDown = 1
        else: 
            legalDown = 0
        if Directions.WEST in gameState.getLegalActions(0):
            legalLenin = 1
        else: 
            legalLenin = 0
        if Directions.EAST in gameState.getLegalActions(0):
            legalFranco = 1
        else: 
            legalFranco = 0

        #Distancia a la primera instancia
        dist = math.sqrt( pow(lightYearsAzul - int(clust[1][0]), 2) + pow(lightYearsNaranja - int(clust[1][1]), 2) + pow(lightYearsCian - int(clust[1][2]), 2)
                    + pow(lightYearsMandarina - int(clust[1][3]), 2) + pow(patrullaX - int(clust[1][4]), 2)*0.5 + pow(patrullaY - int(clust[1][5]), 2)*0.5
                    + pow(legalUp - int(clust[1][6]), 2) + pow(legalDown - int(clust[1][7]), 2) + pow(legalLenin - int(clust[1][8]), 2)
                    + pow(legalFranco - int(clust[1][9]), 2) )
        mov = clust[1][10]
	punt = clust[1][11]
        #Ignora los 2 primeros elementos, el centroide y la distancia ya obtenida
        for inst in clust[2:]:
            #Distancia a la instancia i-esima
            aux = math.sqrt( pow(lightYearsAzul - int(inst[0]), 2) + pow(lightYearsNaranja - int(inst[1]), 2) + pow(lightYearsCian - int(inst[2]), 2)
                    + pow(lightYearsMandarina - int(inst[3]), 2) + pow(patrullaX - int(inst[4]), 2)*0.5 + pow(patrullaY - int(inst[5]), 2)*0.5
                    + pow(legalUp - int(inst[6]), 2) + pow(legalDown - int(inst[7]), 2) + pow(legalLenin - int(inst[8]), 2)
                    + pow(legalFranco - int(inst[9]), 2) )

            #Si la distancia a la instancia actual es menor que la distancia anterior, nos quedamos con el movimiento
            #de esa instancia. Ademas el movimiento es legal
            if (aux <= dist) and (inst[10] in gameState.getLegalActions(0)) and (punt<=inst[11]):
                mov = inst[10]
		punt = inst[11]
                dist = aux
	
	if mov not in gameState.getLegalActions(0):
		mov = Directions.STOP
        return mov



