# bustersAgents.py
# ----------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import util
import os.path
import pprint
import Queue
from game import Agent
from game import Directions
from keyboardAgents import KeyboardAgent
import inference
import busters

import math

class NullGraphics:
    "Placeholder for graphics"
    def initialize(self, state, isBlue = False):
        pass
    def update(self, state):
        pass
    def pause(self):
        pass
    def draw(self, state):
        pass
    def updateDistributions(self, dist):
        pass
    def finish(self):
        pass

class KeyboardInference(inference.InferenceModule):
    """
    Basic inference module for use with the keyboard.
    """
    def initializeUniformly(self, gameState):
        "Begin with a uniform distribution over ghost positions."
        self.beliefs = util.Counter()
        for p in self.legalPositions: self.beliefs[p] = 1.0
        self.beliefs.normalize()

    def observe(self, observation, gameState):
        noisyDistance = observation
        emissionModel = busters.getObservationDistribution(noisyDistance)
        pacmanPosition = gameState.getPacmanPosition()
        allPossible = util.Counter()
        for p in self.legalPositions:
            trueDistance = util.manhattanDistance(p, pacmanPosition)
            if (trueDistance != None ) and (emissionModel[trueDistance] > 0):
                allPossible[p] = emissionModel[trueDistance]
        allPossible.normalize()
        self.beliefs = allPossible

    def elapseTime(self, gameState):
        pass

    def getBeliefDistribution(self):
        return self.beliefs


class BustersAgent:

    turno = 0
    colaDos = Queue.Queue()
    colaCinco = Queue.Queue()
    colaFate = Queue.Queue()

    "An agent that tracks and displays its beliefs about ghost positions."

    def __init__( self, index = 0, inference = "ExactInference", ghostAgents = None, observeEnable = True, elapseTimeEnable = True):
        inferenceType = util.lookup(inference, globals())
        self.inferenceModules = [inferenceType(a) for a in ghostAgents]
        self.observeEnable = observeEnable
        self.elapseTimeEnable = elapseTimeEnable

    def registerInitialState(self, gameState):
        "Initializes beliefs and inference modules"
        import __main__
        self.display = __main__._display
        for inference in self.inferenceModules:
            inference.initialize(gameState)
        self.ghostBeliefs = [inf.getBeliefDistribution() for inf in self.inferenceModules]
        self.firstMove = True

    def observationFunction(self, gameState):
        "Removes the ghost states from the gameState"
        agents = gameState.data.agentStates
        gameState.data.agentStates = [agents[0]] + [None for i in range(1, len(agents))]
        return gameState

    def getAction(self, gameState):
        "Updates beliefs, then chooses an action based on updated beliefs."
        for index, inf in enumerate(self.inferenceModules):
            if not self.firstMove and self.elapseTimeEnable:
                inf.elapseTime(gameState)
            self.firstMove = False
            if self.observeEnable:
                inf.observeState(gameState)
            self.ghostBeliefs[index] = inf.getBeliefDistribution()
        self.display.updateDistributions(self.ghostBeliefs)
        return self.chooseAction(gameState)

    def chooseAction(self, gameState):
        "By default, a BustersAgent just stops.  This should be overridden."
        return Directions.STOP

    #Codigo para guardar los datos. Al ponerlo en esta clase el resto de agentes heredan las funciones.
    def saveData(self,gameState, nombre='default'):
        #width, height = gameState.data.layout.width, gameState.data.layout.height
        if (os.path.isfile(nombre+'.arff') != True):
            self.crearEncabezado(nombre)
        datos = ''
        for living in gameState.livingGhosts:
            datos += str(living)+','
        for distancia in gameState.getNoisyGhostDistances():
            if (distancia == None):
                datos += '-666,'
            else:
                datos += str(distancia)+','
        
        datos += str(gameState.data.agentStates[0].getPosition()[0])+','
        datos += str(gameState.data.agentStates[0].getPosition()[1])+','
        datos += str(gameState.data.score)+',' #Score now

        datos += str(self.prediccion2(gameState))+',' #Score predecida dentro de 2 turnos
        datos += str(self.prediccion5(gameState))+',' #Score predecida dentro de 5 turnos
        #Anchura y altura no son datos de Pac-Man
        #fileHandle.write(str(width) +','+ str(height)+',')
        #Siempre se encola en 2
        self.colaFate.put(str(gameState.data.agentStates[0].getDirection()))
        self.colaDos.put(datos)
        #A partir del turno 2 siempre se desencola de la de 2 y se encola en la de 5,
        if (self.turno >= 2):
            datos = self.colaDos.get()
            datos += str(gameState.data.score)+',' #Puntuacion futura pasada 2
            self.colaCinco.put(datos)
        if (self.turno >= 5):
            datos = self.colaCinco.get()
            datos += str(gameState.data.score)+',' #Puntuacion futura pasada 5
            datos += self.colaFate.get()
            fileHandle = open(nombre+'.arff','a')
            fileHandle.write(datos + '\n')
            fileHandle.close()
        self.turno += 1

    #Funcion para crear el encabezado si el fichero no existia
    def crearEncabezado(self,nombre):
        fileHandle = open(nombre+'.arff','a')
        #relation
        datos = '@relation '+ nombre + '\n\n'
        #atributos
        #   Pacman vivo-muerto/zombie
        datos += '@attribute livingLavidaLocaPacman {True,False}\n'
        #   Fastama Azul
        datos += '@attribute livingLavidaLocaAzul {True,False}\n'
        #   Fantasma Naranja
        datos += '@attribute livingLavidaLocaNaranja {True,False}\n'
        #   Fantasma Cian
        datos += '@attribute livingLavidaLocaCian {True,False}\n'
        #   Fantasma Mandarina
        datos += '@attribute livingLavidaLocaMandarina {True,False}\n'
        #   distancia fantasma Azul
        datos += '@attribute lightYearsAzul numeric\n'
        #   distancia fantasma Naranja
        datos += '@attribute lightYearsNaranja numeric\n'
        #   distancia fantasma Cian
        datos += '@attribute lightYearsCian numeric\n'
        #   distancia fantasma Mandarina
        datos += '@attribute lightYearsMandarina numeric\n'
        #   posicion X pacman
        datos += '@attribute patrullaX numeric\n'
        #   posicion Y pacman
        datos += '@attribute patrullaY numeric\n'
        #   scoreActual
        datos += '@attribute scoreNow numeric\n'
        #   score del futuro pasado a los dos turnos
        datos += '@attribute scorePrediccion2 numeric\n'
        #   score del futuro pasado a los cinco turnos
        datos += '@attribute scorePrediccion5 numeric\n'
        #   score del futuro pasado a los dos turnos
        datos += '@attribute scoreFuture2 numeric\n'
        #   score del futuro pasado a los cinco turnos
        datos += '@attribute scoreFuture5 numeric\n'
        #   direccion (clase) del pacman
        datos += '@attribute fate {North, West, East, South, Stop}\n\n@data\n'

        fileHandle.write(datos + '\n')
        fileHandle.close()

    #Funcion que calcula la estimacion de la puntuacion futura dentro de 2 turnos
    #en base a la regresion lineal obtenida
    def prediccion2(self, gameState):
    # 827.4505 * livingLavidaLocaAzul=True +
    # 212.7191 * livingLavidaLocaNaranja=True +
    # -151.709  * livingLavidaLocaCian=False +
    # 127.177  * livingLavidaLocaMandarina=True +
    # -1.2069 * lightYearsAzul +
    # -0.3119 * lightYearsNaranja +
    # -0.2225 * lightYearsCian +
    # -0.1849 * lightYearsMandarina +
    # 0.9992 * scoreNow +
    # -1134.9039

        #Debido a que al morir la distancia vale None hay que tratarlo
        distancias = []
        for i in gameState.getNoisyGhostDistances():
            if i is None:
                distancias.append(-666)
            else:
                distancias.append(i)

        return math.floor(827.4505 * gameState.livingGhosts[0] 
            + 212.7191 * gameState.livingGhosts[1]  
            -151.709  * -gameState.livingGhosts[2] 
            + 127.177  * gameState.livingGhosts[3]  
            -1.2069 * distancias[0] 
            -0.3119 * distancias[1] 
            -0.2225 * distancias[2] 
            -0.1849 * distancias[3] 
            + 0.9992 * gameState.data.score -1134.9039)

    #Funcion que calcula la estimacion de la puntuacion futura dentro de 5 turnos
    #en base a la regresion lineal obtenida
    def prediccion5(self, gameState):
    #1488.9494 * livingLavidaLocaAzul=True +
    #    646.7221 * livingLavidaLocaNaranja=True +
    #   -438.4839 * livingLavidaLocaCian=False +
    #    260.8185 * livingLavidaLocaMandarina=True +
    #     -2.1638 * lightYearsAzul +
    #     -0.9506 * lightYearsNaranja +
    #     -0.6455 * lightYearsCian +
    #     -0.3763 * lightYearsMandarina +
    #      0.9975 * scoreNow +
    #  -2325.2007
        #Debido a que al morir la distancia vale None hay que tratarlo
        distancias = []
        for i in gameState.getNoisyGhostDistances():
            if i is None:
                distancias.append(-666)
            else:
                distancias.append(i)


        return math.floor(1488.9494 * gameState.livingGhosts[0] + 
            646.7221 * gameState.livingGhosts[1]  
            -438.4839  * -gameState.livingGhosts[2] 
            + 260.8185  * gameState.livingGhosts[3]  
            -2.1638 * distancias[0] 
            -0.9506 * distancias[1] 
            -0.6455 * distancias[2] 
            -0.3763 * distancias[3] 
            + 0.9975 * gameState.data.score -2325.2007)


class BustersKeyboardAgent(BustersAgent, KeyboardAgent):
    "An agent controlled by the keyboard that displays beliefs about ghost positions."


    def __init__(self, index = 0, inference = "KeyboardInference", ghostAgents = None):
        KeyboardAgent.__init__(self, index)
        BustersAgent.__init__(self, index, inference, ghostAgents)

    def getAction(self, gameState):
        return BustersAgent.getAction(self, gameState)

    def chooseAction(self, gameState):
        self.saveData(gameState, 'test_othermaps_keyboard')
        return KeyboardAgent.getAction(self, gameState)


from distanceCalculator import Distancer
from game import Actions
from game import Directions
import random, sys

'''Random PacMan Agent'''
class RandomPAgent(BustersAgent):

    def registerInitialState(self, gameState):
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)
        
    ''' Example of counting something'''
    def countFood(self, gameState):
        food = 0
        for width in gameState.data.food:
            for height in width:
                if(height == True):
                    food = food + 1
        return food

    ''' Print the layout'''  
    def printGrid(self, gameState):
        table = ""
        ##print(gameState.data.layout) ## Print by terminal
        for x in range(gameState.data.layout.width):
            for y in range(gameState.data.layout.height):
                food, walls = gameState.data.food, gameState.data.layout.walls
                table = table + gameState.data._foodWallStr(food[x][y], walls[x][y]) + ","
        table = table[:-1]
        return table

    def printLineData(self,gameState):

        '''Observations of the state
        
        print(str(gameState.livingGhosts))
        print(gameState.data.agentStates[0])
        print(gameState.getNumFood())
        print (gameState.getCapsules())
        width, height = gameState.data.layout.width, gameState.data.layout.height
        print(width, height)
        print(gameState.getNoisyGhostDistances())
        print(gameState.data.layout)'''
      
        '''END Observations of the state'''
        
        print gameState

        self.saveData(gameState, 'training_tutorial1')

        weka_line = ""
        for i in gameState.livingGhosts:
            weka_line = weka_line + str(i) + ","
        weka_line = weka_line + str(gameState.getNumFood()) + "," 
        for i in gameState.getCapsules():
            weka_line = weka_line + str(i[0]) + "," + str(i[1]) + ","
        for i in gameState.getNoisyGhostDistances():
            weka_line = weka_line + str(i) + ","
        weka_line = weka_line + str(gameState.data.score) + "," +\
        str(len(gameState.data.capsules))  + "," + str(self.countFood(gameState)) +\
        "," + str(gameState.data.agentStates[0].configuration.pos[0]) + "," +\
        str(gameState.data.agentStates[0].configuration.pos[0])  +\
        "," + str(gameState.data.agentStates[0].scaredTimer) + "," +\
        self.printGrid(gameState) + "," +\
        str(gameState.data.agentStates[0].numReturned) + "," +\
        str(gameState.data.agentStates[0].getPosition()[0]) + "," +\
        str(gameState.data.agentStates[0].getPosition()[1])+ "," +\
        str(gameState.data.agentStates[0].numCarrying)+ "," +\
        str(gameState.data.agentStates[0].getDirection())
        print(weka_line)
        
        
    def chooseAction(self, gameState):
        move = Directions.STOP
        legal = gameState.getLegalActions(0) ##Legal position from the pacman
        move_random = random.randint(0, 3)
        self.printLineData(gameState)
        if   ( move_random == 0 ) and Directions.WEST in legal:  move = Directions.WEST
        if   ( move_random == 1 ) and Directions.EAST in legal: move = Directions.EAST
        if   ( move_random == 2 ) and Directions.NORTH in legal:   move = Directions.NORTH
        if   ( move_random == 3 ) and Directions.SOUTH in legal: move = Directions.SOUTH
        return move
        
class GreedyBustersAgent(BustersAgent):
    "An agent that charges the closest ghost."

    #variables necesarias para poder decidir la nueva direccion
    oldDistance = -1
    newDistance = 0
    oldDirection = None
    presa = 0

    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)

    def chooseAction(self, gameState):
        """
        First computes the most likely position of each ghost that has
        not yet been captured, then chooses an action that brings
        Pacman closer to the closest ghost (according to mazeDistance!).

        To find the mazeDistance between any two positions, use:
          self.distancer.getDistance(pos1, pos2)

        To find the successor position of a position after an action:
          successorPosition = Actions.getSuccessor(position, action)

        livingGhostPositionDistributions, defined below, is a list of
        util.Counter objects equal to the position belief
        distributions for each of the ghosts that are still alive.  It
        is defined based on (these are implementation details about
        which you need not be concerned):

          1) gameState.getLivingGhosts(), a list of booleans, one for each
             agent, indicating whether or not the agent is alive.  Note
             that pacman is always agent 0, so the ghosts are agents 1,
             onwards (just as before).

          2) self.ghostBeliefs, the list of belief distributions for each
             of the ghosts (including ghosts that are not alive).  The
             indices into this list should be 1 less than indices into the
             gameState.getLivingGhosts() list.
        """
        pacmanPosition = gameState.getPacmanPosition()
        legal = [a for a in gameState.getLegalPacmanActions()]
        livingGhosts = gameState.getLivingGhosts()
        livingGhostPositionDistributions = \
            [beliefs for i, beliefs in enumerate(self.ghostBeliefs)
             if livingGhosts[i+1]]
        "*** YOUR CODE HERE ***"

         #Es necesario presa+1 ya que getLivingGhosts incluye a Pac-Man
        while (gameState.getLivingGhosts()[self.presa+1]==False):
            self.presa += 1

        #Se obtiene la nueva distancia al fantasma
        self.newDistance = gameState.getNoisyGhostDistances()[self.presa]

        #si la nueva distancia es mas cercana o igual al fantasma, sigue la direccion
        if (self.oldDistance >= self.newDistance):
            self.oldDistance = self.newDistance
            legalMoves = gameState.getLegalActions(0)
            if (legalMoves.count(self.oldDirection) == 0):
                self.oldDirection = Directions.STOP                
            self.saveData(gameState,'test_othermaps_tutorial1')
            return self.oldDirection
        else:
            #En caso de no acercarse, si no, alejarse, hay que cambiar direccion
            legalMoves = gameState.getLegalActions(0)
            move_random = random.randint(0, 3)
            #Se evita que la nueva direccion sea la misma que llevaba antes
            if(move_random == 0) and (Directions.WEST in legalMoves) and (Directions.WEST != self.oldDirection):
                self.oldDirection = Directions.WEST
            elif(move_random == 1) and (Directions.EAST in legalMoves) and (Directions.EAST != self.oldDirection):
                self.oldDirection = Directions.EAST
            elif(move_random == 2) and (Directions.NORTH in legalMoves) and (Directions.NORTH != self.oldDirection):
                self.oldDirection = Directions.NORTH
            elif(move_random == 3) and (Directions.SOUTH in legalMoves) and (Directions.SOUTH != self.oldDirection):
                self.oldDirection = Directions.SOUTH
            else:
                #Si no se genera ningun movimiento legal, se queda parado
                self.oldDirection = Directions.STOP
            self.oldDistance = gameState.getNoisyGhostDistances()[self.presa]
            self.saveData(gameState,'test_othermaps_tutorial1')
            return self.oldDirection

#Agente que va a por el fantasma mas cercano
class CloserBustersAgent(BustersAgent):
    "An agent that charges the closest ghost."

    #variables necesarias para poder decidir la nueva direccion
    oldDistance = -1
    newDistance = 0
    oldDirection = None

    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)

    def chooseAction(self, gameState):

        #Se evaluan todas las distancias y se escoge la mas cercana
        distancias = gameState.getNoisyGhostDistances()
        cercana = gameState.data.layout.width * gameState.data.layout.height
        for i in distancias:
            if (i != None) and (i <= cercana):
                cercana = i

        #Se obtiene la nueva distancia al fantasma
        self.newDistance = cercana

        #si la nueva distancia es mas cercana o igual al fantasma, sigue la direccion
        if (self.oldDistance >= self.newDistance):
            self.oldDistance = self.newDistance
            legalMoves = gameState.getLegalActions(0)
            if (legalMoves.count(self.oldDirection) == 0):
                self.oldDirection = Directions.STOP                
            self.saveData(gameState, 'training_tutorial1')
            return self.oldDirection
        else:
            #En caso de no acercarse, si no, alejarse, hay que cambiar direccion
            legalMoves = gameState.getLegalActions(0)
            move_random = random.randint(0, 3)
            #Se evita que la nueva direccion sea la misma que llevaba antes
            if (move_random == 0) and (Directions.WEST in legalMoves) and (Directions.WEST != self.oldDirection):
                self.oldDirection = Directions.WEST
            elif (move_random == 1) and (Directions.EAST in legalMoves) and (Directions.EAST != self.oldDirection):
                self.oldDirection = Directions.EAST
            elif (move_random == 2) and (Directions.NORTH in legalMoves) and (Directions.NORTH != self.oldDirection):
                self.oldDirection = Directions.NORTH
            elif (move_random == 3) and (Directions.SOUTH in legalMoves) and (Directions.SOUTH != self.oldDirection):
                self.oldDirection = Directions.SOUTH
            else:
                #Si no se genera ningun movimiento legal, se queda parado
                self.oldDirection = Directions.STOP
            self.oldDistance = cercana
            self.saveData(gameState, 'training_tutorial1')
            return self.oldDirection


#Agente que va a por el fantasma mas cercano
class j48Agent(BustersAgent):

    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)

    def chooseAction(self, gameState):

        #Movimiento a devolver
        move = None

        #Se recuperan los movimientos legales ya que hay que evitar
        #movimientos indebido como atravesar pareces
        legalMoves = gameState.getLegalActions(0)

        #Se genera el arbol de J48

        patrullaX = gameState.data.agentStates[0].getPosition()[0]
        patrullaY = gameState.data.agentStates[0].getPosition()[1]
        lightYears = gameState.getNoisyGhostDistances()
        livingLavidaLoca = gameState.livingGhosts
        if patrullaX <= 1:
            move = Directions.STOP
        else:
            if livingLavidaLoca[4] is True:
                if lightYears[1] <= 4:
                    if patrullaY <= 13:
                        if patrullaY <= 10:
                            if lightYears[3] <= 15:
                                if patrullaX <= 10:
                                    if livingLavidaLoca[1] is True: 
                                        if patrullaX <= 9:
                                            if patrullaY <= 6: move = Directions.SOUTH
                                            else:
                                                if patrullaX <= 7: move = Directions.SOUTH
                                                else: move = Directions.EAST
                                        else:
                                            if lightYears[3] <= 9: move = Directions.NORTH
                                            else: move = Directions.SOUTH
                                    else:
                                        if patrullaY <= 3: move = Directions.SOUTH
                                        else:
                                            if lightYears[3] <= 7: move = Directions.NORTH
                                            else: move = Directions.EAST
                                else:
                                    if lightYears[3] <= 2: move = Directions.SOUTH
                                    else: move = Directions.EAST
                            else: move = Directions.WEST
                        else: move = Directions.EAST
                    else:
                        if lightYears[0] <= 5: move = Directions.WEST
                        else: move = Directions.SOUTH
                else:
                    if patrullaX <= 14:
                        if patrullaY <= 10: move = Directions.STOP
                        else:
                            if patrullaY <= 11: move = Directions.EAST
                            else: move = Directions.NORTH
                    else:
                        if lightYears[1] <= 14:
                            if patrullaX <= 18:
                                if patrullaX <= 15: move = Directions.NORTH
                                else:
                                    if patrullaX <= 17: move = Directions.WEST
                                    else:
                                        if lightYears[1] <= 9: move = Directions.NORTH
                                        else: move = Directions.EAST
                            else: move = Directions.NORTH
                        else: move = Directions.EAST
            else:
                if lightYears[3] <= 0:
                    if patrullaY <= 8:
                        if patrullaY <= 5:
                            if lightYears[0] <= 9: move = Directions.WEST
                            else: move = Directions.SOUTH
                        else: move = Directions.SOUTH
                    else:
                        if livingLavidaLoca[2] is True:
                            if lightYears[1] <= 6: move = Directions.WEST
                            else: move = Directions.NORTH
                        else:
                            if patrullaX <= 11: move = Directions.WEST
                            else: move = Directions.EAST
                else: move = Directions.SOUTH
        #Para evitar acciones ilegales se elige un movimiento legal al azar
        #cuando no se valido el elegido por el arbol.
        if move not in legalMoves: 
            move = legalMoves[random.randint(0, len(legalMoves)-1)]
        self.saveData(gameState, 'j48Agent')
        return move