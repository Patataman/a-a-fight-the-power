# valueIterationAgents.py
# -----------------------


import mdp, util

from learningAgents import ValueEstimationAgent


class ValueIterationAgent(ValueEstimationAgent):

    tablaQ = []

    def __init__(self, mdp, discount = 0.9, iterations = 100):

        self.readQtable()
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0

    def getValue(self, state):
        """
            Return the value of the state (computed in __init__).
        """
        return self.values[state]

    def computeQValueFromValues(self, state, action):
        """
        Compute the Q-value of action in state from the
        value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"
        estado = str(state[0]) +','+ str(state[1]) #estado = X,Y
        for i in self.tablaQ:
            #Se encuentra el estado
            if i[0] == estado:
                #Se devuelve el refuerzo asociado a la accion
                if action == 'north':
                    return i[1][0][2]
                elif action == 'south':
                    return i[1][1][2]
                elif action == 'west':
                    return i[1][2][2]
                else: #east
                    return i[1][3][2]

    def computeActionFromValues(self, state):
        """
        The policy is the best action in the given state
        according to the values currently stored in self.values.

        You may break ties any way you see fit.  Note that if
        there are no legal actions, which is the case at the
        terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        if self.mdp.isTerminal(state): #Se entiende que devuelve True(Meta) o False(No meta) 
            return None
        else:
            estado = str(state[0]) +','+ str(state[1]) #estado = X,Y
            for i in self.tablaQ:
                #Devuelve el par Estado | Acciones
                if i[0] == estado:
                    #Se guarda la primera accion y refuerzo asociado
                    auxAc = i[1][0][0]
                    auxR = i[1][0][2]
                    for j in i[1][0:]:
                    #Si el refuerzo de otra accion el mayor al refuerzo guardado se
                    #actualiza el refuerzo y la accion
                        if j[2] > auxR:
                            auxAc = j[0]
                            auxR = j[2]
                    #Como solo hay una unica coincidencia con el estado, una vez se 
                    #encuentra y evaluan las acciones se devuelve
                    return auxAc

    def readQtable(self):
        auxQ = open('tablaQAAGrid','r').read()
        #Cada fila es un estado, la primera columna es el identificador del estado, las demas
        #son los refuerzos de cada accion
        auxQ = auxQ.split('\n') #Array de tantas filas como estados
        for i in auxQ:
            fila = i.split(':') #0 = estado, 1 = acciones
            fila[0] = fila[0] #Identificador del estado
            fila[1] = fila[1].split(';')
            fila[1][0] = fila[1][0].split('_') 
            #fila[i][j][0] = accion
            #fila[i][j][1] = estado'
            #fila[i][j][2] = refuerzo
            fila[1][0][2] = float(fila[1][0][2])
            fila[1][1] = fila[1][1].split('_')
            fila[1][1][2] = float(fila[1][1][2])
            fila[1][2] = fila[1][2].split('_')
            fila[1][2][2] = float(fila[1][2][2])
            fila[1][3] = fila[1][3].split('_')
            fila[1][3][2] = float(fila[1][3][2])
            self.tablaQ.append(fila)

    def writeQtable(self):
        fileHandle = open('tablaQAAGrid2','w')
        for i in self.tablaQ:
            acciones = ''
            w = 0
            for j in i[1]:
                if w != 3:
                    w += 1
                    acciones += str(j[0])+'-'+str(j[1])+'-'+str(j[2])+';'
                else:
                    w=0
                    acciones += str(j[0])+'-'+str(j[1])+'-'+str(j[2])+'\n'
            fileHandle.write(i[0]+':'+acciones)
        fileHandle.close()
        
    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)