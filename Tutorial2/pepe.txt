supervisado -> intancia -> resample

biasTouniformClass -> 1
randomSeed -> Disinto de 1
-XmxNumeroM Con Numero = memoria en MBytes que se quiere asignar

Para normalizar: Filtros-> No supervisado -> Atributos -> Normalizar

Filtro addExpression: Es una especie de atajo. Distancia del fantasma noseq con el nosecuantos y
se necesita la suma, pero no la habíamos guardado en los datos.
Para referenciar atributos se escribe: aX expresion aY
Y nos agrega el nuevo atributo.


Hay algoritmos que no se pueden aplicar dependiendo del conjunto de datos que tengas.
Mirar capacidades del algoritmo (click encima del nombre una vez seleccionado), no todos valen para todo.

Filtros importantes, el balanceo, normalizar, addExpresion, discretizar (transformar atributos numéricos a rangos)