# bustersAgents.py
# ----------------

import util
from game import Agent
from game import Directions
from keyboardAgents import KeyboardAgent
import inference
import busters

class NullGraphics:
    "Placeholder for graphics"
    def initialize(self, state, isBlue = False):
        pass
    def update(self, state):
        pass
    def pause(self):
        pass
    def draw(self, state):
        pass
    def updateDistributions(self, dist):
        pass
    def finish(self):
        pass

class KeyboardInference(inference.InferenceModule):
    """
    Basic inference module for use with the keyboard.
    """
    def initializeUniformly(self, gameState):
        "Begin with a uniform distribution over ghost positions."
        self.beliefs = util.Counter()
        for p in self.legalPositions: self.beliefs[p] = 1.0
        self.beliefs.normalize()

    def observe(self, observation, gameState):
        noisyDistance = observation
        emissionModel = busters.getObservationDistribution(noisyDistance)
        pacmanPosition = gameState.getPacmanPosition()
        allPossible = util.Counter()
        for p in self.legalPositions:
            trueDistance = util.manhattanDistance(p, pacmanPosition)
            if emissionModel[trueDistance] > 0:
                allPossible[p] = 1.0
        allPossible.normalize()
        self.beliefs = allPossible

    def elapseTime(self, gameState):
        pass

    def getBeliefDistribution(self):
        return self.beliefs


class BustersAgent:
    "An agent that tracks and displays its beliefs about ghost positions."

    def __init__( self, index = 0, inference = "ExactInference", ghostAgents = None, observeEnable = True, elapseTimeEnable = True):
        inferenceType = util.lookup(inference, globals())
        self.inferenceModules = [inferenceType(a) for a in ghostAgents]
        self.observeEnable = observeEnable
        self.elapseTimeEnable = elapseTimeEnable

    def registerInitialState(self, gameState):
        "Initializes beliefs and inference modules"
        import __main__
        self.display = __main__._display
        for inference in self.inferenceModules:
            inference.initialize(gameState)
        self.ghostBeliefs = [inf.getBeliefDistribution() for inf in self.inferenceModules]
        self.firstMove = True

    def observationFunction(self, gameState):
        "Removes the ghost states from the gameState"
        #agents = gameState.data.agentStates
        #gameState.data.agentStates = [agents[0]] + [None for i in range(1, len(agents))]
        return gameState

    def getAction(self, gameState):
        "Updates beliefs, then chooses an action based on updated beliefs."
        for index, inf in enumerate(self.inferenceModules):
            if not self.firstMove and self.elapseTimeEnable:
                inf.elapseTime(gameState)
            self.firstMove = False
            if self.observeEnable:
                inf.observeState(gameState)
            self.ghostBeliefs[index] = inf.getBeliefDistribution()
        self.display.updateDistributions(self.ghostBeliefs)
        return self.chooseAction(gameState)

    def chooseAction(self, gameState):
        "By default, a BustersAgent just stops.  This should be overridden."
        return Directions.STOP

class BustersKeyboardAgent(BustersAgent, KeyboardAgent):
    "An agent controlled by the keyboard that displays beliefs about ghost positions."

    def __init__(self, index = 0, inference = "KeyboardInference", ghostAgents = None):
        KeyboardAgent.__init__(self, index)
        BustersAgent.__init__(self, index, inference, ghostAgents)

    def getAction(self, gameState):
        return BustersAgent.getAction(self, gameState)

    def chooseAction(self, gameState):
        return KeyboardAgent.getAction(self, gameState)

from distanceCalculator import Distancer
from game import Actions
from game import Directions
import random, sys

'''Random PacMan Agent'''
class RandomPAgent(BustersAgent):

    def registerInitialState(self, gameState):
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)
        
    ''' Example of counting something'''
    def countFood(self, gameState):
        food = 0
        for width in gameState.data.food:
            for height in width:
                if(height == True):
                    food = food + 1
        return food
    
    ''' Print the layout'''  
    def printGrid(self, gameState):
        table = ""
        ##print(gameState.data.layout) ## Print by terminal
        for x in range(gameState.data.layout.width):
            for y in range(gameState.data.layout.height):
                food, walls = gameState.data.food, gameState.data.layout.walls
                table = table + gameState.data._foodWallStr(food[x][y], walls[x][y]) + ","
        table = table[:-1]
        return table
        
    def printLineData(self,gameState):
    
        '''Observations of the state
        
        print(str(gameState.livingGhosts))
        print(gameState.data.agentStates[0])
        print(gameState.getNumFood())
        print (gameState.getCapsules())
        width, height = gameState.data.layout.width, gameState.data.layout.height
        print(width, height)
        print(gameState.data.ghostDistances)
        print(gameState.data.layout)'''
      
        '''END Observations of the state'''
        
        print gameState
        
        weka_line = ""
        for i in gameState.livingGhosts:
            weka_line = weka_line + str(i) + ","
        weka_line = weka_line + str(gameState.getNumFood()) + "," 
        for i in gameState.getCapsules():
            weka_line = weka_line + str(i[0]) + "," + str(i[1]) + ","
        for i in gameState.data.ghostDistances:
            weka_line = weka_line + str(i) + ","
        weka_line = weka_line + str(gameState.data.score) + "," +\
        str(len(gameState.data.capsules))  + "," + str(self.countFood(gameState)) +\
        "," + str(gameState.data.agentStates[0].configuration.pos[0]) + "," +\
        str(gameState.data.agentStates[0].configuration.pos[0])  +\
        "," + str(gameState.data.agentStates[0].scaredTimer) + "," +\
        self.printGrid(gameState) + "," +\
        str(gameState.data.agentStates[0].numReturned) + "," +\
        str(gameState.data.agentStates[0].getPosition()[0]) + "," +\
        str(gameState.data.agentStates[0].getPosition()[1])+ "," +\
        str(gameState.data.agentStates[0].numCarrying)+ "," +\
        str(gameState.data.agentStates[0].getDirection())
        print(weka_line)
        
        
    def chooseAction(self, gameState):
        move = Directions.STOP
        legal = gameState.getLegalActions(0) ##Legal position from the pacman
        move_random = random.randint(0, 3)
        self.printLineData(gameState)
        if   ( move_random == 0 ) and Directions.WEST in legal:  move = Directions.WEST
        if   ( move_random == 1 ) and Directions.EAST in legal: move = Directions.EAST
        if   ( move_random == 2 ) and Directions.NORTH in legal:   move = Directions.NORTH
        if   ( move_random == 3 ) and Directions.SOUTH in legal: move = Directions.SOUTH
        return move
        
from learningAgents import ReinforcementAgent

"""
      Q-Learning Agent

      Instance variables you have access to
        - self.epsilon (exploration prob)
        - self.alpha (learning rate)
        - self.discount (discount rate) gamma

      Functions you should use
        - self.getLegalActions(state)
          which returns legal actions for a state
"""

import math, random, os.path
        
class ImpresionPruebaBorrar(BustersAgent):
    "An agent that charges the closest ghost."
    """
        These default parameters can be changed from the pacman.py command line.
        For example, to change the exploration rate, try:
           
        QTable   - QTable
        alpha    - learning rate
        epsilon  - exploration rate
        gamma    - discount factor
        numTraining - number of training episodes, i.e. no learning after these many episodes
        """
    tablaQ = []
    lastDir = Directions.STOP
    prevState = ""
    prevAction = ""
    prevScore = 0
    alpha = 0
    gamma = 0
    countdown = 1000
        
    def registerInitialState(self, gameState):
        "Pre-computes the distance between every two points."
        BustersAgent.registerInitialState(self, gameState)
        self.distancer = Distancer(gameState.data.layout, False)
        self.alpha = 1
        self.gamma = 0.9
        self.cargarTabla()
        #Numero maximo de acciones
        self.countdown = 1000

    def __del__(self):
        #Se guarda la tabla
        self.escribirTabla()
        
    def update(self, prevState, prevAction, estadoQ, prevScore, score, fantasmas):
        """
          The parent class calls this to observe a
          state = action => nextState and reward transition.
          You should do your Q-Value update here

          NOTE: You should never call this function,
          it will be called on your behalf
        """
        "*** YOUR CODE HERE ***"

        #Q(s,a) = (1-alpha)*Q(s,a) + alpha[r + gamma*maxQ(s',a')]
        qvalor = 0
        r = 0
        #Si se come un fantasma el refuerzo es 100.
        if prevScore < score:
            for i in fantasmas:
                if i == False:
                    r += 10
        #Refuerzos intermedios
        else:
            acciones = prevState.split(',')
            #Por cada accion legal se otorga 1 de refuerzo
            for i in acciones[:4]:
                if i == "1":
                    r += 1
            #Solo una accion legal
            if r == 1:
                r = -30
            else:
                #Si la accion realizada fue un movimiento a la direccion del fantasma, refuerzo extra.
                if acciones[4] == prevAction:
                    r += 10
                elif r >= 3:
                    if acciones[4] == "South" and prevAction == "North":
                        r = -10
                    elif acciones[4] == "North" and prevAction == "South":
                        r = -10
                    elif acciones[4] == "West" and prevAction == "East":
                        r = -10
                    elif acciones[4] == "East" and prevAction == "West":
                        r = -10

        for i in self.tablaQ:
            if i[0] == estadoQ:
                for w in range(0,4):
                    if i[1][w] > qvalor:
                        #Nos quedamos con el mayor refuerzo del estado siguiente
                        qvalor = i[1][w]

                #Sale del 1er for
            break
        #Hay que actualizar el refuerzo de prevState en la tabla Q
        fila = 0
        if r != 0 or qvalor != 0:
            for i in self.tablaQ:
                #Se encuentra el estado
                if i[0] == prevState:
                    if prevAction == Directions.NORTH:
                        self.tablaQ[fila][1][0] = (1-self.alpha)*self.tablaQ[fila][1][0] + self.alpha*(r + self.gamma*qvalor)
                    elif prevAction == Directions.EAST:
                        self.tablaQ[fila][1][1] = (1-self.alpha)*self.tablaQ[fila][1][1] + self.alpha*(r + self.gamma*qvalor)
                    elif prevAction == Directions.SOUTH:
                        self.tablaQ[fila][1][2] = (1-self.alpha)*self.tablaQ[fila][1][2] + self.alpha*(r + self.gamma*qvalor)
                    elif prevAction == Directions.WEST:
                        self.tablaQ[fila][1][3] = (1-self.alpha)*self.tablaQ[fila][1][3] + self.alpha*(r + self.gamma*qvalor)
                    #Una vez se actualiza la tabla, se sale del bucle
                    break;
                fila += 1
            #Se va reduciendo la importancia de alpha.
            self.alpha = self.alpha*0.9

    def getAction(self, state):
        """
        The policy is the best action in the given state
        according to the values currently stored in self.values.

        You may break ties any way you see fit.  Note that if
        there are no legal actions, which is the case at the
        terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"

        if self.countdown < 0:
            print "YOU HAVE FAILED THIS GAME (city)"
            exit()

        #Genera la tupla de la tablaQ de los movimientos legales
        legalMoves = state.getLegalActions(0)


        estadoQ = ""
        if Directions.NORTH in legalMoves:
            estadoQ += "1,"
        else:
            estadoQ += "0,"
        if Directions.EAST in legalMoves:
            estadoQ += "1,"
        else:
            estadoQ += "0,"
        if Directions.SOUTH in legalMoves:
            estadoQ += "1,"
        else:
            estadoQ += "0,"
        if Directions.WEST in legalMoves:
            estadoQ += "1,"
        else:
            estadoQ += "0,"

        estadoQ += str(self.getDirection(state))#Obtenemos la direccion del fantasma mas cercano

        score = state.data.score
        if self.prevState != "":
            self.update(self.prevState, self.prevAction, estadoQ, self.prevScore, score, state.livingGhosts)

        for i in self.tablaQ:
            #Si dado un estado, existe en la tabla se devuelve la accion que da mayor refuerzo
            if estadoQ == i[0]:
                mejor = float("-inf")
                acc = Directions.STOP
                for w in range(0,4):
                    #Seria un epsilon de 0.8
                    if random.random() < 0.2:
                        acc = legalMoves[int(random.random()*len(legalMoves))]
                        break
                    if i[1][w] == mejor:
                        #Si son iguales hay un 50% de que se cambie de accion
                        if random.random() > 0.5:
                            if w == 0 and Directions.NORTH in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.NORTH
                            if w == 1 and Directions.EAST in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.EAST
                            if w == 2 and Directions.SOUTH in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.SOUTH
                            if w == 3 and Directions.WEST in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.WEST
                    elif i[1][w] > mejor:
                            if w == 0 and Directions.NORTH in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.NORTH
                            if w == 1 and Directions.EAST in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.EAST
                            if w == 2 and Directions.SOUTH in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.SOUTH
                            if w == 3 and Directions.WEST in legalMoves: 
                                mejor = i[1][w]
                                acc = Directions.WEST
                self.prevState = estadoQ
                self.prevAction = acc
                self.prevScore = state.data.score
                self.countdown -= 1
                return acc

        #Escogemos uno de los movimientos legales
        acc = legalMoves[int(random.random()*len(legalMoves))] 
        #guardamos los datos para poder actualizar la tablaQ
        self.prevState = estadoQ
        self.prevAction = acc
        self.prevScore = state.data.score

        #Si el estado no existe, se crea en la tabla y se elige una accion aleatoria
        insertState = [estadoQ, [0,0,0,0]]
        self.tablaQ.append(insertState)
        self.countdown -= 1
        return acc

    def getDirection(self,gameState):
        #Devuelve la direccion del fantasma mas cercano
        pacman_azul = math.sqrt(math.pow(gameState.data.agentStates[0].getPosition()[0] - gameState.getGhostPosition(1)[0],2) +
                                 math.pow(gameState.data.agentStates[0].getPosition()[1] - gameState.getGhostPosition(1)[1],2))
        pacman_naranja = math.sqrt(math.pow(gameState.data.agentStates[0].getPosition()[0] - gameState.getGhostPosition(2)[0],2) +
                                 math.pow(gameState.data.agentStates[0].getPosition()[1] - gameState.getGhostPosition(2)[1],2))
        pacman_cian = math.sqrt(math.pow(gameState.data.agentStates[0].getPosition()[0] - gameState.getGhostPosition(3)[0],2) +
                                 math.pow(gameState.data.agentStates[0].getPosition()[1] - gameState.getGhostPosition(3)[1],2))
        pacman_mandarina = math.sqrt(math.pow(gameState.data.agentStates[0].getPosition()[0] - gameState.getGhostPosition(4)[0],2) +
                                 math.pow(gameState.data.agentStates[0].getPosition()[1] - gameState.getGhostPosition(4)[1],2))
        mejor = 0
        distMin = 900
        vivos = []
        for i in gameState.livingGhosts:
            vivos.append(i)
        for x in range(1, 5):
            if vivos[x] != False:
                distAux = math.sqrt(math.pow(gameState.data.agentStates[0].getPosition()[0] - gameState.getGhostPosition(x)[0],2) +
                                     math.pow(gameState.data.agentStates[0].getPosition()[1] - gameState.getGhostPosition(x)[1],2))
                if distAux < distMin:
                    mejor = x
                    distMin = distAux

        distVert = gameState.data.agentStates[0].getPosition()[1] - gameState.getGhostPosition(mejor)[1]
        distHor = gameState.data.agentStates[0].getPosition()[0] - gameState.getGhostPosition(mejor)[0]
        #La direccion horizontal es la mas cercana
        if math.fabs(distVert) < math.fabs(distHor):
            if distHor > 0:
                #moverse izquierda
                return Directions.WEST
            else:
                #moverse derecha
                return Directions.EAST
        #La direccion vertical es la mas cercana
        else:
            if distVert > 0:
                #moverse izquierda
                return Directions.SOUTH
            else:
                #moverse derecha
                return Directions.NORTH

    def cargarTabla(self):
        if os.path.isfile('tablaQImpresionPruebaBorrar') == True:
            self.tablaQ = []
            fileHandle = open('tablaQImpresionPruebaBorrar','r')
            for linea in fileHandle:
                filaTablaQ = []
                estado = linea.split(';')[0]
                qvalores = linea.split(';')[1].split(',')
                for w in range(0,4):
                    qvalores[w] = float(qvalores[w])
                filaTablaQ.append(estado)
                filaTablaQ.append(qvalores)
                self.tablaQ.append(filaTablaQ)
            fileHandle.close()

    def escribirTabla(self):
        fileHandle = open('tablaQImpresionPruebaBorrar','w')
        for i in self.tablaQ:
            qvalores = ""
            for q in range(0,4):
                if q == 4:
                    qvalores += "%s" % i[1][q]
                else:
                    qvalores += "%s," % i[1][q]

            fileHandle.write('%s;%s\n' % (i[0], qvalores))
        fileHandle.close()